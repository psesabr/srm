# Detect proper script_path (you cannot use args yet as they are build with tools in set_env.r)
script_path <- (function() {
  args <- commandArgs(trailingOnly = FALSE)
  script_path <- dirname(sub("--file=", "", args[grep("--file=", args)]))
  if (!length(script_path)) {
    return("R")
  }
  if (grepl("darwin", R.version$os)) {
    base <- gsub("~\\+~", " ", base) # on MacOS ~+~ in path denotes whitespace
  }
  return(normalizePath(script_path))
})()

base_dir <- script_path # do tego folderu będą lądować wyniki
script_path <- normalizePath(file.path(base_dir, "..", "02. Aplikacja", "R"))

# Setting .libPaths() to point to libs folder
source(file.path(script_path, "set_env.R"), chdir = TRUE)

config <- load_config()
args <- args_parser()

suppressPackageStartupMessages({
  library(SRM.Config)
  library(SRM.DB)
  library(SRM.Validation)
})

# ładowanie konfiguracji
#------------------------------------------------------------------------------
env_conf <- list()
env_conf$APP_ROOT <- normalizePath(file.path(script_path, ".."), winslash = "/")
env_conf$COMMON_CONFIG_DIR <- file.path(env_conf$APP_ROOT, config$COMMON_CONFIG_DIR)
env_conf$USER_CONFIG_DIR <- file.path(Sys.getenv('HOME'), "SRM", "settings")
SRM.Config::load_app_config(env_conf)


# Przygotowanie raportu
#------------------------------------------------------------------------------
dummy_folder_name <- NULL
#dummy_folder_name <- "walidacja_2018-11-10_10.20"
folder <- SRM.Validation::init_report_generation(base_dir, dummy_folder_name = dummy_folder_name)

valid_log <- SRM.Validation::SRM.Validation_getLogger()
valid_log$log(logging::loglevels[['INFO']], "Wczytywanie danych z bazy danych ...")

conn <- SRM.Config::get_conn()
plan <- SRM.DB::get_plan_tab(conn, start_point = 0, day_ix = 0)
rownames(plan) <- sprintf("%s %d %04d", plan$ts, 0, start_point)
wykon <- SRM.DB::get_tab(conn, tab = "wykon")
rownames(wykon) <- sprintf("%s %d %04d", wykon$ts, 0, start_point)
moc_os <- SRM.DB::get_tab(conn, tab = "moc_os")
rownames(moc_os) <- sprintf("%s %d %04d", moc_os$ts, 0, start_point)

distr_obj <- SRM.Config::get_distr_obj()
distr_def <- SRM.Utils::distr_obj_get_distr(distr_obj, 0, 0)

if (is.null(dummy_folder_name)) {
  pdf_fpath <- SRM.Validation::generate_validation_report(plan, wykon, moc_os, distr_def, folder)
} else {
  pdf_fpath <- SRM.Validation::generate_dummy_report(plan, wykon, moc_os, folder)
}

logging::loginfo("Report generated at %s. Openning ...", pdf_fpath)
SRM.Validation::open_pdf_report(pdf_fpath)

logging::loginfo("All done.")
