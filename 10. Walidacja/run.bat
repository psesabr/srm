@echo off

rem
rem Detecting if Rscript is available. If not it will be searched in registry
rem

Rscript.exe -e "stopifnot(startsWith(paste0(R.version$major, R.version$minor), '33.'))" 1> nul 2>&1
if ERRORLEVEL 1 (
	echo Rscript v3.3 is not available at path. Trying to detect in registry ...

	echo ... detecting per-user R installation ...
	for /f "skip=1 tokens=2* " %%a in ('reg query HKCU\SOFTWARE\R-core\R /v InstallPath /s 2^> nul') do (
		set "PATH=%%~sb\bin;%PATH%"
		Rscript.exe -e "stopifnot(startsWith(paste0(R.version$major, R.version$minor), '33.'))" 1> nul 2>&1
		if NOT ERRORLEVEL 1 (
			echo ... found at %%~sb\bin
			goto rscript_found
		)
	)
	echo ... not found

	echo ... detecting global R installation ...
	for /f "skip=1 tokens=2* " %%a in ('reg query HKLM\SOFTWARE\R-core\R /v InstallPath /s 2^> nul') do (
		set "PATH=%%~sb\bin;%PATH%"
		Rscript.exe -e "stopifnot(startsWith(paste0(R.version$major, R.version$minor), '33.'))" 1> nul 2>&1
		if NOT ERRORLEVEL 1 (
			echo ... found at %%~sb\bin
			goto rscript_found
		)
	)
	echo ... not found

    echo ERROR: Failed to detect R 3.3 path in registry. R 3.3 is required to run SRM.
    echo Please install R 3.3 and verify its location added PATH environment variable.
    exit /B 1
)

:rscript_found

set base_dir=%~dps0
Rscript.exe --no-init-file "%base_dir%/master.R"
