CREATE TABLE IF NOT EXISTS `punkty_startowe` (
  `punkt` INTEGER PRIMARY KEY
);

INSERT INTO `punkty_startowe`(`punkt`) VALUES (   0);
INSERT INTO `punkty_startowe`(`punkt`) VALUES ( 900);
INSERT INTO `punkty_startowe`(`punkt`) VALUES (1300);
INSERT INTO `punkty_startowe`(`punkt`) VALUES (1600);


ALTER TABLE `plan` ADD COLUMN `punkt_startowy` INTEGER NOT NULL DEFAULT(0);
ALTER TABLE `plan` ADD COLUMN `dzien_ix` INTEGER NOT NULL DEFAULT(0);
