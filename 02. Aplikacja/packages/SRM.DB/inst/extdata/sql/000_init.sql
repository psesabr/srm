CREATE TABLE IF NOT EXISTS `moc_os` (
  `ts` TEXT,
  `moc_os_JWCDc` REAL,
  `moc_os_JWCDw` REAL,
  `moc_os_nJWCD_bez_wiatr` REAL,
  `moc_os_wiatr` REAL
);

CREATE TABLE IF NOT EXISTS `plan` (
  `ts` TEXT,
  `zapotrz_KSE_plan` REAL,
  `gener_nJWCD_bez_wiatr_plan` REAL,
  `gener_wiatr_plan` REAL,
  `saldo_wym_plan` REAL,
  `dysp_nJWCD_bez_wiatr_plan` REAL,
  `dysp_JWCDc_plan` REAL
);

CREATE TABLE IF NOT EXISTS `wykon` (
  `ts` TEXT,
  `zapotrz_KSE_wykon` REAL,
  `gener_nJWCD_bez_wiatr_wykon` REAL,
  `gener_wiatr_wykon` REAL,
  `saldo_wym_wykon` REAL,
  `dysp_nJWCD_bez_wiatr_wykon` REAL,
  `dysp_JWCDc_wykon` REAL,
  `dysp_JWCDw_wykon` REAL
);
