#include <Rcpp.h>
using namespace Rcpp;

//' Funkcja c_merge_orderd_vectors (C++)
//'
//' Funkcja pomocnicza tworząca:
//' (weight = 0) posortowany wektor z elementów x i y
//' (weight = 1) posortowany wektor z elementów x i y,
//'              podmieniając wartości x na wagę 1/length(x),
//'              wartości y na wagę 1/length(y)
//'
//' @param x wektor numeryczny
//' @param y wektor numeryczny
//' @param weight wartość determinująca działanie funkcji (czytaj opis funkcji)
//'
//' @return wektor numeryczny:
//'         (weight = 0) wartości z elementów x i y
//'         (weight = 1) wagi: 1/length(x) lub 1/length(y) w kolejności
//'                      elementów x i y (patrz opis funkcji)
//'
//' @rdname c_merge_orderd_vectors
// [[Rcpp::export]]
NumericVector c_merge_orderd_vectors(NumericVector x,NumericVector y,int weight) {
    int nx = x.size();
    int ny = y.size();
    NumericVector ret(nx+ny);

    if(weight==0){

        int i = 0;
        int j = 0;

        for(int l=0;l<(nx+ny);l++){

            if(i<nx and j<ny){
                if(y[j]<x[i]){
                    ret[l] = y[j];
                    j++;
                }else{
                    ret[l] = x[i];
                    i++;
                }

            }else if(j<ny){
                ret[l] = y[j];
                j++;
            }else if(i<nx){
                ret[l] = x[i];
                i++;
            };

        };

    }else{

        double inv_nx = 1.0/nx;
        double min_inv_ny = (-1.0)/ny;

        int i = 0;
        int j = 0;

        for(int l=0;l<(nx+ny);l++){

            if(i<nx and j<ny){
                if(y[j]<x[i]){
                    ret[l] = min_inv_ny;
                    j++;
                }else{
                    ret[l] = inv_nx;
                    i++;
                }

            }else if(j<ny){
                ret[l] = min_inv_ny;
                j++;
            }else if(i<nx){
                ret[l] = inv_nx;
                i++;
            };
        };
    };

    return ret;
}
