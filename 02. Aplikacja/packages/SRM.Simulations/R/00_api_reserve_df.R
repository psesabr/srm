#
# PSE S.A.
# 2018
#
# Funkcje związane z obliczeniami poziomu rezerw niezbędnych do zbilansowania
# systemu z określonym prawdopodobieństwem
#

#'
#' Funkcja generująca macierze wartości rezerwy.
#'
#' @param perc wybrane poziomy prawdopodobieństwa
#' @param plan_df obiekt data.frame zawierający wartości prognozowane zmiennych
#' @param moc_os obiekt data.frame zawierający wartości mocy osiągalnych zmiennych
#' @param mc_sample wielkość próby dla symulacji Monte Carlo
#' @param kappa lista wartości przesuniec
#' @param distr_obj obiekt zawierający definicje rozkładów (distr_obj).
#' @param seed wartość ziarna losowego dla symulacji MC
#' @param start_f funkcja o sygnaturze function(task_no, core_no), która
#'   informuje o rozpoczęciu równoległych obliczeń.
#'
#' @return macierze wartości rezerwy
#'
#' @export
#'
reserve_df <- function(perc, plan_df, moc_os, mc_sample, kappa, distr_obj,
                       seed = NULL, start_f = function(task_no, core_no) NULL) {

    # struktura tabeli
    nr <- nrow(plan_df)

    # moc osiągalna
    j_bar <- moc_os$moc_os_JWCDc
    n_bar <- moc_os$moc_os_nJWCD_bez_wiatr
    w_bar <- moc_os$moc_os_wiatr
    z_hat <- as.numeric(plan_df[, "zapotrz_KSE_plan"])

    ts_desc <- rownames(plan_df)
    ts_desc <- gsub("/", "-", ts_desc)

    # przeliczenie q_features
    q_features_ts2 <- q_features(ts_desc)

    # informujemy o rozpoczęciu obliczeń w nr taskach
    core_no <- detectCores() - 1
    start_f(nr, core_no)

    # ustawianie seed
    if (is.null(seed)) seed <- 10291039
    set.seed(seed)
    seedi <- sample(1:10 ^ 8, nr)

    cl <- makeCluster(spec = core_no)
    clusterEvalQ(cl, {
        loadNamespace("SRM.Utils")
        loadNamespace("SRM.Simulations")
    })
    clusterExport(cl, c("q_features_ts2", "z_hat", "j_bar", "n_bar", "w_bar",
                        "mc_sample", "kappa", "perc", "seedi", "distr_obj"),
                  envir = environment())

    ret_list <- parLapply(
        cl = cl,
        X = seq_len(nr),
        fun = function(i) {

            # ustawienie ziarna losowego
            set.seed(seedi[i])

            out <- matrix(NA, nrow = 1, 8)
            out[1, 1] <- i

            # przeliczenie wyzej q_feauters
            ts_i <- q_features_ts2[i, ]

            # pobranie odpowiedniego rozkładu
            distr_def <- distr_obj_get_distr(distr_obj, start_point = ts_i[[6]], day_ix = ts_i[[5]])
            stopifnot(!is.null(distr_def))

            ykde <- SRM.Simulations::kde_y(ts_i, z_hat[i], j_bar[i], n_bar[i], w_bar[i],
                                           n = mc_sample, distr_def = distr_def)

            out[1, 7] <- max(0, SRM.Simulations::calc_reserve(ykde, perc, kappa$KAPPA_Y))
            out[1, 8] <- round(out[1, 7] / z_hat[i] * 100, 1)

            zkde <- SRM.Simulations::kde_z(ts_i, distr_def$gamma_z)
            mkde <- SRM.Simulations::kde_m(ts_i, distr_def$epsilon_m)
            wkde <- SRM.Simulations::kde_w(ts_i, distr_def$eta_w)
            nkde <- SRM.Simulations::kde_n(ts_i, distr_def$eta_n)
            jkde <- SRM.Simulations::kde_j(ts_i, distr_def$eta_j)

            res <- c(SRM.Simulations::calc_reserve_z(kde_z = zkde, z_hat[i],
                                                     perc, kappa$KAPPA_Z),
                     SRM.Simulations::calc_reserve_m(kde_m = mkde,
                                                     perc, kappa$KAPPA_M),
                     SRM.Simulations::calc_reserve_wjn(kde_x = wkde, x_bar = w_bar[i],
                                                       perc, kappa$KAPPA_W),
                     SRM.Simulations::calc_reserve_wjn(kde_x = nkde, x_bar = n_bar[i],
                                                       perc, kappa$KAPPA_N),
                     SRM.Simulations::calc_reserve_wjn(kde_x = jkde, x_bar = j_bar[i],
                                                       perc, kappa$KAPPA_J))
            res <- pmax(res, 0)
            if (sum(res) == 0) {
                out[1, 2:6] <- 1 / 5 * out[1, 7]
            } else {
                out[1, 2:6] <- res / sum(res) * out[1, 7]
            }
            out[1, 2:6] <- res / sum(res) * out[1, 7]

            return(data.frame(out))
        })

    stopCluster(cl)

    out <- as.matrix(as.data.frame(rbindlist(ret_list)))
    out <- out[order(out[, 1]), ]
    out1 <- out[, -1]
    colnames(out1) <- c("Zapotrzebowanie KSE",
                        "Saldo wymiany",
                        "Generacja wiatrowa",
                        "Generacja nJWCD*",
                        "DYSP_JWCDc pracujacych",
                        "rezerwa",
                        "rezerwa/plan [%]")


    nh <- nr %/% 4
    ts_out <- character(nh)
    temp <- matrix(NA, nh, 7)
    colnames(temp) <- c("Zapotrzebowanie KSE",
                        "Saldo wymiany",
                        "Generacja wiatrowa",
                        "Generacja nJWCD*",
                        "DYSP_JWCDc pracujacych",
                        "rezerwa",
                        "rezerwa/plan [%]")
    for (i in 1:nh) {
        starti <- 1 + 4 * (i - 1)
        ts_out[i] <- substr(ts_desc[starti + 3], 1, 13)
        temp[i, ] <- round(pmax(out1[starti, ], out1[starti + 1, ],
                                out1[starti + 2, ], out1[starti + 3, ]))
        temp[i, "rezerwa/plan [%]"] <-
            round(temp[i, "rezerwa"] / mean(z_hat[starti:(starti + 3)]) * 100, 1)
    }


    out1 <- temp
    ii <- which(duplicated(ts_out))
    if (length(ii) > 0) ts_out[ii] <- paste0(ts_out[ii], "A")
    ts_out <- paste0(ts_out, ":00")
    rownames(out1) <- ts_out

    return(out1)
}


#'
#' Funkcja generująca macierze wartości rezerwy (sekwencyjnie).
#'
#' @param perc wybrane poziomy prawdopodobieństwa
#' @param plan_df obiekt data.frame zawierający wartości prognozowane zmiennych
#' @param moc_os obiekt data.frame zawierający wartości mocy osiągalnych zmiennych
#' @param mc_sample wielkość próby dla symulacji Monte Carlo
#' @param kappa lista wartości przesunięć
#' @param distr_obj obiekt zawierający definicje rozkładów (distr_obj)
#' @param seed wartość ziarna losowego dla symulacji MC
#'
#' @return macierze wartości rezerwy
#'
#' @export
#'
reserve_df_sequential <- function(perc, plan_df, moc_os, mc_sample, kappa, distr_obj,
                                  seed = NULL) {
    # struktura tabeli
    nr <- nrow(plan_df)
    out1 <- matrix(NA, nr, 7)
    colnames(out1) <- c("Zapotrzebowanie KSE",
                        "Saldo wymiany",
                        "Generacja wiatrowa",
                        "Generacja nJWCD*",
                        "DYSP_JWCDc pracujacych",
                        "rezerwa",
                        "rezerwa/plan [%]")

    # moc osiągalna
    j_bar <- moc_os$moc_os_JWCDc
    n_bar <- moc_os$moc_os_nJWCD_bez_wiatr
    w_bar <- moc_os$moc_os_wiatr
    z_hat <- as.numeric(plan_df[, "zapotrz_KSE_plan"])

    ts_desc <- rownames(plan_df)
    ts_desc <- gsub("/", "-", ts_desc)

    # MK - przeliczenie q_features
    q_features_ts2 <- q_features(ts_desc)

    # ustawianie seed
    if (is.null(seed)) seed <- 10291039
    set.seed(seed)
    seedi <- sample(1:10 ^ 8, nr)

    for (i in seq_len(nr)) {

        # ustawienie ziarna losowego
        set.seed(seedi[i])

        # przeliczenie wyzej q_feauters
        ts_i <- q_features_ts2[i, ]

        # pobranie odpowiedniego rozkładu
        distr_def <- distr_obj_get_distr(distr_obj, start_point = ts_i[[6]], day_ix = ts_i[[5]])
        stopifnot(!is.null(distr_def))

        ykde <- kde_y(ts_i, z_hat[i], j_bar[i], n_bar[i], w_bar[i],
                      n = mc_sample, distr_def = distr_def)

        out1[i, "rezerwa"] <- pmax(0, calc_reserve(ykde, perc, kappa$KAPPA_Y))
        out1[i, "rezerwa/plan [%]"] <- round(out1[i, "rezerwa"] / z_hat[i] * 100, 1)

        zkde <- kde_z(ts_i, distr_def$gamma_z)
        mkde <- kde_m(ts_i, distr_def$epsilon_m)
        wkde <- kde_w(ts_i, distr_def$eta_w)
        nkde <- kde_n(ts_i, distr_def$eta_n)
        jkde <- kde_j(ts_i, distr_def$eta_j)
        res <- c(calc_reserve_z(kde_z = zkde, z_hat[i], perc, kappa$KAPPA_Z),
                 calc_reserve_m(kde_m = mkde, perc, kappa$KAPPA_M),
                 calc_reserve_wjn(kde_x = wkde, x_bar = w_bar[i], perc, kappa$KAPPA_W),
                 calc_reserve_wjn(kde_x = nkde, x_bar = n_bar[i], perc, kappa$KAPPA_N),
                 calc_reserve_wjn(kde_x = jkde, x_bar = j_bar[i], perc, kappa$KAPPA_J))
        res <- pmax(res, 0)
        if (sum(res) == 0) {
            out1[i, 1:5] <- 1 / 5 * out1[i, "rezerwa"]
        } else {
            out1[i, 1:5] <- res / sum(res) * out1[i, "rezerwa"]
        }
        out1[i, 1:5] <- res / sum(res) * out1[i, "rezerwa"]
    }

    nh <- nr %/% 4
    ts_out <- character(nh)
    temp <- matrix(NA, nh, 7)
    colnames(temp) <- c("Zapotrzebowanie KSE",
                        "Saldo wymiany",
                        "Generacja wiatrowa",
                        "Generacja nJWCD*",
                        "DYSP_JWCDc pracujacych",
                        "rezerwa",
                        "rezerwa/plan [%]")
    for (i in 1:nh) {
        starti <- 1 + 4 * (i - 1)
        ts_out[i] <- substr(ts_desc[starti + 3], 1, 13)
        temp[i, ] <- round(pmax(out1[starti, ], out1[starti + 1, ],
                                out1[starti + 2, ], out1[starti + 3, ]))
        temp[i, "rezerwa/plan [%]"] <-
            round(temp[i, "rezerwa"] / mean(z_hat[starti:(starti + 3)]) * 100, 1)
    }
    out1 <- temp
    ii <- which(duplicated(ts_out))
    if (length(ii) > 0) ts_out[ii] <- paste0(ts_out[ii], "A")
    ts_out <- paste0(ts_out, ":00")
    rownames(out1) <- ts_out
    return(out1)
}
