#
# PSE S.A.
# 2018
#
# Funkcje obsługujące mechanizm Monte-Carlo
#


#'
#' Estymacja funkcji gęstości rozkładu dla bilansu.
#'
#' @param qf znacznik czasowy określający kwadrans (4 elementy: godz, mc, typ,
#'           typ dnia następnego)
#' @param z_hat wartość planowana (zapotrzebowanie)
#' @param j_bar moc zainstalowana (JWCD)
#' @param n_bar moc zainstalowana (nJWCD)
#' @param w_bar moc zainstalowana (el. wiatrowe)
#' @param distr_def definicja rozkładu (lista nazwana)
#' @param n wielkość próby
#'
#' @return rozkład teoretyczny bilansu
#'
#' @export
#'
kde_y <- function(qf, z_hat, j_bar, n_bar, w_bar, distr_def, n = 5e5) {

    # definicja rozkładów
    gamma_z <- distr_def$gamma_z
    eta_w <- distr_def$eta_w
    epsilon_m <- distr_def$epsilon_m
    eta_j <- distr_def$eta_j
    eta_n <- distr_def$eta_n

    h <- qf[1]
    m <- qf[2]
    work <- qf[3]
    work1 <- qf[4]

    # zapotrzebowanie
    if (work == 1 & work1 == 1) hh <- h
    if (work == 1 & work1 == 0) hh <- 24 + h
    if (work == 0 & work1 == 1) hh <- 2 * 24 + h
    if (work == 0 & work1 == 0) hh <- 3 * 24 + h
    z_q_ind <- (m - 1) * 24 * 4 + hh
    kde_z <- gamma_z$kde_list[[gamma_z$map[z_q_ind]]]

    # wiatr
    w_q_ind <- (m - 1) * 24 + h
    kde_w <- eta_w$kde_list[[eta_w$map[w_q_ind]]]

    # saldo
    mjn_q_ind <- (m - 1) * 24 * 2 - ((work - 1) * 24) + h
    kde_m <- epsilon_m$kde_list[[epsilon_m$map[mjn_q_ind]]]

    # JWCD
    kde_j <- eta_j$kde_list[[eta_j$map[mjn_q_ind]]]

    # nJWCD
    kde_n <- eta_n$kde_list[[eta_n$map[mjn_q_ind]]]

    # Y
    spl_y <- sample_y(kde_z, z_hat, kde_m, kde_j, j_bar, # z 12_reseve_helpers.R
                      kde_n, n_bar, kde_w, w_bar, n)
    y_kde <- kde(spl_y, binned = TRUE)

    return(y_kde)
}


#'
#' Estymacja funkcji gęstości rozkładu błędu prognozy zapotrzebowania.
#'
#' @param qf znacznik czasowy określający kwadrans (4 elementy: godz, mc, typ,
#'           typ dnia następnego)
#' @param gamma_z składnik gamma_z z definicji rozkładu
#'
#' @return rozkład teoretyczny
#'
#' @export
#'
kde_z <- function(qf, gamma_z) {

    h <- qf[1]
    m <- qf[2]
    work <- qf[3]
    work1 <- qf[4]

    # zapotrzebowanie
    z_map <- gamma_z$map
    z_kde <- gamma_z$kde_list
    if (work == 1 & work1 == 1) hh <- h
    if (work == 1 & work1 == 0) hh <- 24 + h
    if (work == 0 & work1 == 1) hh <- 2 * 24 + h
    if (work == 0 & work1 == 0) hh <- 3 * 24 + h
    z_q_ind <- (m - 1) * 24 * 4 + hh
    z_group <- z_map[z_q_ind]
    kde_z <- z_kde[[z_group]]

    return(kde_z)
}


#'
#' Estymacja funkcji gęstości rozkładu błędu prognozy salda wymiany międzynarodowej.
#'
#' @param qf znacznik czasowy określający kwadrans (4 elementy: godz, mc, typ, typ
#'        dnia następnego)
#' @param epsilon_m składnik epsilon_m z definicji rozkładu
#'
#' @return rozkład teoretyczny
#'
#' @export
#'
kde_m <- function(qf, epsilon_m) {

    h <- qf[1]
    m <- qf[2]
    work <- qf[3]

    mjn_q_ind <- (m - 1) * 24 * 2 - ((work - 1) * 24) + h
    # saldo
    m_map <- epsilon_m$map
    m_kde <- epsilon_m$kde_list
    m_group <- m_map[mjn_q_ind]
    kde_m <- m_kde[[m_group]]

    return(kde_m)
}


#'
#' Estymacja funkcji gęstości rozkładu błędu prognozy generacji wiatrowej.
#'
#' @param qf znacznik czasowy określający kwadrans (4 elementy: godz, mc, typ,
#'           typ dnia następnego)
#' @param eta_w składnik eta_w z definicji rozkładu
#'
#' @return rozkład teoretyczny
#'
#' @export
#'
kde_w <- function(qf, eta_w) {

    h <- qf[1]
    m <- qf[2]

    # wiatr
    w_map <- eta_w$map
    w_kde <- eta_w$kde_list
    w_q_ind <- (m - 1) * 24 + h
    w_group <- w_map[w_q_ind]
    kde_w <- w_kde[[w_group]]

    return(kde_w)
}


#'
#' Estymacja funkcji gęstości rozkładu błędu prognozy mocy dyspozycyjnej JWCDc.
#'
#' @param qf znacznik czasowy określający kwadrans (4 elementy: godz, mc, typ,
#'           typ dnia następnego)
#' @param eta_j składnik eta_j z definicji rozkładu
#'
#' @return rozkład teoretyczny
#'
#' @export
#'
kde_j <- function(qf, eta_j) {

    h <- qf[1]
    m <- qf[2]
    work <- qf[3]

    mjn_q_ind <- (m - 1) * 24 * 2 - ((work - 1) * 24) + h
    # JWCD
    j_map <- eta_j$map
    j_kde <- eta_j$kde_list
    j_group <- j_map[mjn_q_ind]
    kde_j <- j_kde[[j_group]]

    return(kde_j)
}


#'
#' Estymacja funkcji gęstości rozkładu błędu prognozy prognozy generacji
#' źródeł nJWCD.
#'
#' @param qf znacznik czasowy określający kwadrans (4 elementy: godz, mc, typ,
#'           typ dnia następnego)
#' @param eta_n składnik eta_n z definicji rozkładu
#'
#' @return rozkład teoretyczny
#'
#' @export
#'
kde_n <- function(qf, eta_n) {

    h <- qf[1]
    m <- qf[2]
    work <- qf[3]

    mjn_q_ind <- (m - 1) * 24 * 2 - ((work - 1) * 24) + h
    # nJWCD
    n_map <- eta_n$map
    n_kde <- eta_n$kde_list
    n_group <- n_map[mjn_q_ind]
    kde_n <- n_kde[[n_group]]

    return(kde_n)
}


#'
#' Obliczenie poziomu rezerwy łącznej dla prawdopodobieństwa nieprzekroczenia
#' bilansu.
#'
#' @param kde_y rozkład teoretyczny
#' @param prob wybrane poziomy prawdopodobieństwa
#' @param kappa wartość przesunięcia dla rozkładu
#'
#' @return poziomy rezerwy
#'
#' @export
#'
calc_reserve <- function(kde_y, prob, kappa) {

    res <- icdf_kde(p = prob, kde_y) - kappa
    return(res)
}


#'
#' Obliczenie poziomu rezerwy dla prawdopodobieństwa nieprzekroczenia
#' odpowiadającej poziomowi zapotrzebowania.
#'
#' @param kde_z rozkład teoretyczny
#' @param z_hat prognoza zapotrzebowania
#' @param prob wybrane poziomy prawdopodobieństwa
#' @param kappa wartość przesunięcia dla rozkładu
#'
#' @return poziomy rezerwy
#'
#' @export
#'
calc_reserve_z <- function(kde_z, z_hat, prob, kappa) {

    res <- (icdf_kde(p = prob, kde_z) - 1) * z_hat - kappa
    return(res)
}


#'
#' Obliczenie poziomu rezerwy dla prawdopodobieństwa nieprzekroczenia
#' odpowiadającej poziomowi salda wymiany międzynarodowej.
#'
#' @param kde_m rozkład teoretyczny
#' @param prob wybrane poziomy prawdopodobieństwa
#' @param kappa wartość przesunięcia dla rozkładu
#'
#' @return poziomy rezerwy
#'
#' @export
#'
calc_reserve_m <- function(kde_m, prob, kappa) {

    res <- -icdf_kde(p = (1 - prob), fhat = kde_m) - kappa
    return(res)
}


#'
#' Obliczenie poziomu rezerwy dla prawdopodobieństwa nieprzekroczenia
#' odpowiadającej poziomowi generacji wiatrowej /
#' mocy dyspozycyjnej źródeł JWCD / generacji źródeł nJWCD.
#'
#' @param kde_x rozkład teoretyczny
#' @param x_bar moc zainstalowana
#' @param prob wybrane poziomy prawdopodobieństwa
#' @param kappa wartość przesunięcia dla rozkładu
#'
#' @return poziomy rezerwy
#'
#' @export
#'
calc_reserve_wjn <- function(kde_x, x_bar, prob, kappa) {

    res <-  -x_bar * (icdf_kde(p = (1 - prob), fhat = kde_x)) - kappa
    return(res)
}


#'
#' Funkcja odwrotnej dystrybuanty.
#'
#' @param p wybrane poziomy prawdopodobieństwa
#' @param fhat funkcja gęstości (obiekt KDE)
#'
#' @return wartości kwantyli
#'
#' @export
#'
icdf_kde <- function(p, fhat) {

    # wykorzystanie funkcji sprzetowej, a jesli ta nie zadziala, wlasnej definicji
    my_try <- try({
        res <- qkde(p, fhat)
    },
    silent = TRUE)

    if (inherits(my_try, "try-error")) {
        ab <- range(c(fhat$x, fhat$eval.points))
        a <- ab[1]
        b <- ab[2]
        n <- 2000
        x <- seq(a, b, length.out = n)

        cdf <- pkde(x, fhat)
        np <- length(p)
        a <- rep(1, np)
        b <- rep(n, np)


        while (any(b - a > 1)) {
            c <- round((a + b) / 2)
            gt <- which(cdf[c] > p)
            lt <- which(cdf[c] < p)
            eq <- which(cdf[c] == p)

            b[gt] <- c[gt]
            a[lt] <- c[lt]
            a[eq] <- c[eq]
            b[eq] <- c[eq]
        }
        res <- x[a]
    }
    return(res)
}
