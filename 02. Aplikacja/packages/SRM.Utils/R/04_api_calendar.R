#
# PSE S.A.
# 2018
#
# Funkcje związane z czasem i kalendarzem aplikacji obliczeniowej
#


#'
#' Określenie typu dnia (h, mc, typ, typ dnia następnego).
#'
#' Funkcja identyfikuje godziny (1:24), miesiąc (1:12),
#' rodzaj dnia (0 - wolny, 1 - roboczy), rodzaj dnia następnego (0/1)
#' oraz indeks dnia w kontekście horyzontu prognozy.
#'
#' @param quarter wektor znaczników czasowych określający konkretny kwadrans
#'        (obiekt typu character), np. "2018-11-09 0315 01"
#'
#' @return macierz z 6 kolumnami typu integer, identyfikujący dla poszczególnych
#'         znaczników:
#'         1) godzinę (1-24)
#'         2) miesiąc (1-12)
#'         3) typ dnia (0- wolny, 1 - roboczy)
#'         4) typ dnia następnego (0- wolny, 1 - roboczy)
#'         5) indeks dnia (0 - prognoza na ten sam dzień, 1 - prognoza na dzień naprzód itd.)
#'         6) punkt startowy - godzinę w formie liczbowej (np. 0900)
#'
#' @export
#'
q_features <- function(quarter) {

    features <- matrix(rep(0, 6 * length(quarter)), ncol = 6)

    # pasmo godzinowe
    hour <- as.integer(substr(quarter, 12, 13))
    quart <- substr(quarter, 14, 15)

    features[, 1] <- hour
    idx <- quart != "00"
    features[idx, 1] <- features[idx, 1] + 1

    # charakterystyki dnia
    features[, 2:4] <- h_features(substr(quarter, 1, 10))
    # indeks dnia i punkt startowy
    features[, 5:6] <- horizon_features(quarter)
    return(features)
}


#'
#' Wrapper dla q_features zwracający ramkę danych z nazwanymi kolumnami zamiast macierzy.
#'
#' @param quarter wektor znaczników czasowych określający konkretny kwadrans
#'        (obiekt typu character), np. "2018-11-09 0315 01"
#'
#' @return ramka danych z 5 kolumnami typu integer o nazwach hour, month, workingday, nextworkingday, day_idx, start_point
#'         identyfikująca dla poszczególnych znaczników:
#'         1) godzinę (1-24)
#'         2) miesiąc (1-12)
#'         3) typ dnia (0- wolny, 1 - roboczy)
#'         4) typ dnia następnego (0- wolny, 1 - roboczy)
#'         5) indeks dnia (0 - prognoza na ten sam dzień, 1 - prognoza na dzień naprzód itd.)
#'         6) punkt startowy - godzinę w formie liczbowej (np. 0900)
#'
#' @export
#'
q_features_df <- function(quarter) {
    features <- data.frame(q_features(quarter))
    names(features) <- c("hour", "month", "workingday", "nextworkingday", "day_idx", "start_point")
    return(features)
}

#'
#' Określenie indeksu dnia w kontekście horyzontu prognozy (dane z prognozy na ten sam dzień,
#' na 1 dzień do przodu itd.) oraz punktu startowego.
#'
#' @param quarter wektor znaczników czasowych określający konkretny kwadrans
#'        (obiekt typu character), np. "2018-11-09 0315 1 0900"
#'
#' @return macierz z 2 kolumnami typu integer, identyfikująca dla poszczególnych
#'         znaczników indeks dnia oraz punkt startowy; zwraca 0 w przypadku braku
#'
#' @export
#'
horizon_features <- function(quarter) {
    features <- matrix(rep(0, 2 * length(quarter)), ncol = 2)
    start_point_rgx <- " [0-9]{4}$"
    # punkt startowy
    features[, 2] <- vapply(quarter, function(q) {
        rgx_match <- regmatches(q, regexpr(start_point_rgx, q))
        ifelse(length(rgx_match) > 0, as.integer(rgx_match), 0)}, FUN.VALUE = 0)
    # indeks dnia
    features[, 1] <- vapply(quarter, function(q) {
        as.integer(trimws(gsub(start_point_rgx, "", substr(q, 16, nchar(q)))))},
                            FUN.VALUE = 0)
    features[is.na(features)] <- 0
    return(features)
}

#'
#' Określenie typu dnia (mc, typ, typ dnia następnego).
#'
#' Funkcja identyfikuje miesiąc (1:12), rodzaj dnia (0 - wolny, 1 - roboczy)
#' i rodzaj dnia następnego (0:1).
#'
#' @param date wektor znaczników czasowych określający konkretny kwadrans (obiekt typu character)
#'
#' @return macierz z 3 kolumnami typu integer, identyfikujący dla poszczególnych znaczników:
#'         1) miesiąc (1-12)
#'         2) typ dnia (0- wolny, 1 - roboczy)
#'         3) typ dnia następnego (0- wolny, 1 - roboczy)
#'
#' @export
#'
h_features <- function(date) {

    features0 <- matrix(rep(0, 3 * length(date)), ncol = 3)
    # miesiąc
    m <- as.integer(substr(date, 6, 7))
    features0[, 1] <- m


    # dzień roboczy czy wolny
    y <- as.integer(substr(date, 1, 4))
    d <- as.integer(substr(date, 9, 10))
    datint <- ((y * 100 + m) * 100) + d
    features0[, 2] <- working_day(datint)

    # dzień następny roboczy czy wolny
    day1 <- increment_d(y, m, d)

    datint1 <- ((day1[, 1] * 100 + day1[, 2]) * 100) + day1[, 3]
    features0[, 3] <- working_day(datint1)

    return(features0)
}


#'
#' Określa, czy dany dzień jest roboczy.
#'
#' @param d wektor dat (numeryczny)
#'
#' @return wektor numeryczny (0 - dzień wolny, 1 - dzień roboczy)
#'
#' @export
#'
working_day <- function(d) {

    # funkcje pomocnicze (przesunięcie daty)
    add_1 <- function(y, md) {
        res <- y * 10 ^ 4 + (md == 331) * 401 + (md != 331) * (md + 1)
        return(res)
    }
    add_49 <- function(y, md) {
        res <- y * 10 ^ 4 + ((md > 412 | md <= 331)
                              * (188 + md) + (md > 331 & md <= 412) * (119 + md))
        return(res)
    }
    add_60 <- function(y, md) {
        res <- y * 10 ^ 4 + ((md <= 331 | md > 401)
                              * (199 + md) + (md == 401) * 531)
        return(res)
    }
    if (length(d) == 0)
        return(integer(0))
    y <- d %/% 10 ^ 4
    mmdd <- d %% 10 ^ 4

    # święta
    sat_sun <- (dayofweek(d) > 5)
    new_year <- (mmdd == 101)
    king3 <- (mmdd == 106) & (y <= 1960 | y >= 2011)
    easter_date <- easter(y)
    emd <- easter_date %% 10 ^ 4
    east <- (d == add_1(y, emd))
    may1 <- (mmdd == 501)
    may3 <- (mmdd == 503) & (y >= 1990)
    pentecost <- (d == add_49(y, emd))
    carpus_chri <- (d == add_60(y, emd))
    renaisse <- (mmdd == 722) & (y < 1990)
    assump_bvm <- (mmdd == 815) & (y <= 1960 | y >= 1989)
    all_saints <- (mmdd == 1101)
    dep <- (mmdd == 1111) & (y >= 1990)
    christm <- (mmdd == 1225)
    christm2 <- (mmdd == 1226)
    not_work <- (sat_sun | new_year | king3 | east | may1 |
                     may3 | pentecost | carpus_chri | assump_bvm | all_saints |
                     dep | christm | christm2 | renaisse)
    res <- matrix(0 + 1 * (!not_work), ncol = 1)
    rownames(res) <- d
    colnames(res) <- "work_day"

    return(res)
}


#'
#' Określenie dnia tygodnia.
#'
#' @param d wektor dat (numeryczny)
#'
#' @return wektor numeryczny okreslający dzień tygodnia poszczególnych dat:
#'         1 - pon,
#'         2 - wt,
#'         ...
#'         7 - nd
#'
#' @export
#'
dayofweek <- function(d) {

    y <- d %/% 10 ^ 4
    m <- ((d %% 10 ^ 4) %/% 10 ^ 2)
    d <- (d %% 10 ^ 2)
    a <- (14 - m) %/% 12
    y <- y - a
    m <- m + 12 * a - 2
    d <- (d + y + y %/% 4 - y %/% 100 + y %/% 400 + (31 * m) %/% 12) %% 7
    d <- d + (d == 0) * 7
    return(d)
}


#'
#' Określenie daty Wielkanocy.
#'
#' @param y wektor określający rok (numeryczny)
#'
#' @return wektor numeryczny okreslający daty Wielkanocy w poszczególnych latach
#'
#' @export
#'
easter <- function(y) {
    g <- y %% 19
    cc <- y %/% 100
    h <- (cc - cc %/% 4 - (8 * cc + 13) %/% 25 + 19 * g + 15) %% 30
    i <- h - (h %/% 28) * (1 - (29 %/% (h + 1)) * ((21 - g) %/% 11))
    j <- (y + y %/% 4 + i + 2 - cc + cc %/% 4) %% 7
    l <- i - j
    m <- 3 + (l + 40) %/% 44
    d <- l + 28 - 31 * (m %/% 4)
    res <- y * 10 ^ 4 + m * 10 ^ 2 + d
    return(res)
}


#'
#' Liczba dni w miesiącu.
#'
#' @param y wektor określający rok (numeryczny)
#' @param m wektor określający mc (numeryczny)
#'
#' @return wektor numeryczny określający liczbę dni w poszczególnych miesiącach
#'
#' @export
#'
no_d <- function(y, m) {

    days <- rep(30, length(y))
    days[m %in% c(1, 3, 5, 7, 8, 10, 12)] <- 31
    days[m == 2] <- 28
    days[(m == 2) & (((y %% 4 == 0) & (y %% 100 != 0)) || (y %% 400 == 0))] <- 29

    return(days)
}


#'
#' Zwiększenie daty o 1 dzień.
#'
#' @param y wektor określający rok (numeryczny)
#' @param m wektor określający mc (numeryczny)
#' @param d wektor określający dzień (numeryczny)
#'
#' @return wektor numeryczny z datą zwiększoną o 1 dzień
#'
#' @export
#'
increment_d <- function(y, m, d) {

    nod <- no_d(y, m)

    y1 <- y
    m1 <- m
    d1 <- d + 1

    idx_nod <- d == nod
    idx_m <- m == 12

    idx <- idx_nod & idx_m
    y1[idx] <- y[idx] + 1
    m1[idx] <- 1
    d1[idx] <- 1

    idx <- idx_nod & !idx_m
    y1[idx] <- y[idx]
    m1[idx] <- m[idx] + 1
    d1[idx] <- 1


    return(cbind(y1, m1, d1))
}


#'
#' Zmniejszenie daty o 1 dzień.
#'
#' @param y wektor określający rok (numeryczny)
#' @param m wektor określający mc (numeryczny)
#' @param d wektor określający dzień (numeryczny)
#'
#' @return wektor numeryczny z datą zmniejszoną o 1 dzień
#'
#' @export
#'
decrement_d <- function(y, m, d) {

    if (d == 1) {
        if (m == 1) {
            y_1 <- y - 1
            m_1 <- 12
            d_1 <- 31
        } else {
            y_1 <- y
            m_1 <- m - 1
            d_1 <- no_d(y_1, m_1)
        }
    } else {
        y_1 <- y
        m_1 <- m
        d_1 <- d - 1
    }
    return(c(y_1, m_1, d_1))
}
