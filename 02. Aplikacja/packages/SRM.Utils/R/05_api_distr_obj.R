#
# PSE S.A.
# 2018
#
# Implementacja obiektu do zarządzania definicjami rozkładów
#

#'
#' Buduje obiekt distr_obj na podstawie załadowanego środowiska.
#'
#' @param distr_env środowisko zawierające definicje rozkładów
#'
#' @return utworzony obiekt distr_obj
#'
#' @export
#'
distr_obj_build <- function(distr_env) {
    assert(is.environment(distr_env))

   res <- list(distribs = list())
   class(res) <- "distr_obj"

   version <- distr_env$version
   if (is.null(version)) {
       # stara wersja: zakładamy, że zawiera rozkłady dla start_point == 0 i date_ix == 0
       res$distribs[["0000_0"]] <- mget(c("gamma_z", "eta_w", "epsilon_m", "eta_j", "eta_n"),
                                        envir = distr_env,
                                        ifnotfound = list(NULL, NULL, NULL, NULL, NULL))
   } else if (version == 0) {
       assert(exists("distribs", envir = distr_env))
       res$distribs <- get("distribs", envir = distr_env)
   }

   for (distr_key in names(res$distribs)) {
       distr_def <- res$distribs[[distr_key]]
       if (is.character(distr_def)) {
           # to jest informacja o błędzie: nie wymaga sprawdzania
           next
       }

       .check_rd(distr_def, distr_key)
   }

   res$update_dates <- distr_env$distr_update_dates

   return(res)
}


#'
#' Tworzy pusty obiekt do umieszczenia definicji rozkładów.
#'
#' @return pusty obiekt typu distr_obj
#'
#' @export
#'
distr_obj_create_empty <- function() {
    res <- list(distribs = list(),
                update_dates = character(2))
    class(res) <- "distr_obj"

    res$update_dates[1] <- as.character(Sys.Date())
    res$update_dates[2] <- ""
    names(res$update_dates) <- c("Data_ostatniej_aktualizacji",
                                 "Data_ostatniej_obserwacji")

    return(res)
}

#'
#' Funkcja walidująca zupełność definicji pojedyńczej definicji rozkładu.
#'
#' Generuje wyjątek (srm_err), jeśli wykryje nieprawidłowości.
#'
#' @param distr_def lista zawierająca definicję rozkładu
#' @param distr_key klucz rozkładu, który specyfikuje punkt startowy i indeks dnia,
#'   którego rozkład dotyczy
#'
#' @return invisible(NULL)
#'
#' @keywords internal
#' @noRd
#'
.check_rd <- function(distr_def, distr_key = "0000_0") {
    common_rd_vars <- c("gamma_z", "eta_w", "epsilon_m", "eta_j", "eta_n")
    miss <- NULL

    # czy wszystkie sa
    miss <- setdiff(common_rd_vars, names(distr_def))
    # braki
    if (length(miss) > 0) {
        stop(srm_err("COMMON_RD_VARS_MISSING", x = miss, y = distr_key))
    }

    # sprawdzenie typu danych
    ti <- NULL
    for (i in seq_along(common_rd_vars)) {
        var_name <- common_rd_vars[[i]]
        vi <- distr_def[[var_name]]
        if (is.null(vi) || class(vi) != "list")
            ti <- c(ti, var_name)
    }

    # zly typ
    if (length(ti) > 0) {
        stop(srm_err("COMMON_RD_VARS_TYPES_WRONG", x = ti, y = distr_key))
    }
}

#'
#' Tworzy środowisko do zapisania do pliku z definicjami rozkładów.
#'
#' @param distr_obj obiekt zawierający definicje rozkładów (distr_obj)
#'
#' @return środowisko zawierające zmienne opisujące rozkłady i informacje administracyjne
#'
#' @export
#'
distr_obj_export_env <- function(distr_obj) {
    assert(is_distr_obj(distr_obj))

    res <- new.env(parent = emptyenv())

    res$version <- 0
    res$distr_update_dates <- distr_obj$update_dates
    res$distribs <- distr_obj$distribs

    return(res)
}

#'
#' Pobiera z obiektu opisującego rozkłady definicję rozkładów (distr_obj) dla podanego
#' punktu startowego i indeksu dnia.
#'
#' @param distr_obj obiekt opisujący rozkłady (distr_obj)
#' @param start_point punkt startowy (np. 0, 300, 900, 1600, typ: integer(1))
#' @param day_ix indeks dnia (typ: integer(1))
#'
#' @return lista nazwana z parametrami rozkładu
#'
#' @export
#'
distr_obj_get_distr <- function(distr_obj, start_point, day_ix) {
    assert(is_distr_obj(distr_obj))

    distr_key <- sprintf("%04d_%d", start_point, day_ix)
    distr <- distr_obj$distribs[[distr_key]]
    if (is.null(distr)) {
        return(NULL)
    }

    if (is.character(distr)) {
        # to jest informacja o błędzie
        return(NULL)
    }

    return(distr)
}

#'
#' Ustawia definicję rozkładu dla zdefiniowanego puntu startowego i indeksu dnia.
#'
#' Weryfikuje rozkład przed ustawieniem, czy zawiera niezbędne zmienne. Jeśli
#' wykrywa błędy w definicji rozkładu, generuje błąd (srm_err).
#'
#' Aktualizuje dane rozkładu w elemencie update_dates: ustawia datę aktualizacji i datę
#' ostatniej próbki zgodnie z podanym prametrem.
#'
#' @param distr_obj obiekt z definicjami rozkładów (distr_obj)
#' @param distr_def definicja rozkładu (lista nazwana z parametrami rozkładu)
#' @param last_db_date stempel czasowy ostatniej próbki danych,
#'    na których był policzony rozkład (type: character(1))
#' @param start_point punkt startowy (np. 0, 300, 900, 1600, typ: integer(1))
#' @param day_ix indeks dnia (typ: integer(1))
#'
#' @return uaktualniony obiekt z definicjami rozkładów (distr_obj)
#'
#' @export
#'
distr_obj_set_distr <- function(distr_obj, distr_def, last_db_date, start_point, day_ix) {
    assert(is_distr_obj(distr_obj))

    distr_key <- sprintf("%04d_%d", start_point, day_ix)
    .check_rd(distr_def, distr_key)

    distr_obj$distribs[[distr_key]] <- distr_def
    distr_obj$update_dates[[1]] <- as.character(Sys.Date())
    distr_obj$update_dates[[2]] <- last_db_date

    return(distr_obj)
}

#'
#' Ustawia w objekcie opisującym rozkłady informację o błędzie podczas generacji rozkładu
#' dla zdefiniowanego puntu startowego i indeksu dnia.
#'
#' @param distr_obj obiekt z definicjami rozkładów (distr_obj)
#' @param error błąd do ustawienia (character)
#' @param last_db_date stempel czasowy ostatniej próbki danych,
#'    na których był policzony rozkład (type: character(1))
#' @param start_point punkt startowy (np. 0, 300, 900, 1600, typ: integer(1))
#' @param day_ix indeks dnia (typ: integer(1))
#'
#' @return uaktualniony obiekt z definicjami rozkładów (distr_obj)
#'
#' @export
#'
distr_obj_set_error <- function(distr_obj, error, last_db_date, start_point, day_ix) {
    assert(is_distr_obj(distr_obj))
    assert(is.character(error))

    distr_key <- sprintf("%04d_%d", start_point, day_ix)

    distr_obj$distribs[[distr_key]] <- error
    distr_obj$update_dates[[1]] <- as.character(Sys.Date())
    distr_obj$update_dates[[2]] <- last_db_date

    return(distr_obj)
}

#'
#' Pobiera informacje o błędach podczas generacji rozkładów.
#'
#' @param distr_obj obiekt z definicjami rozkładów (distr_obj)
#'
#' @return lista nazwana z indentyfikatorami typy XXXX_X i błędami
#'
#' @export
#'
distr_obj_get_errors <- function(distr_obj) {
    assert(is_distr_obj(distr_obj))

    errors <- list()
    for (distr_key in names(distr_obj$distribs)) {
        distr_def <- distr_obj$distribs[[distr_key]]
        if (is.character(distr_def)) {
            errors[[distr_key]] <- distr_def
        }
    }
    return(errors)
}

#'
#' Pobiera indeksy dni, dla których brakuje rozkładów.
#'
#' @param distr_obj obiekt z definicjami rozkładów (distr_obj)
#' @param start_point punkt startowy (np. 0, 300, 900, 1600, typ: integer(1))
#' @param day_idxs indeksy dni do sprawdzenia (typ: integer(N))
#'
#' @return wektor indeksów dni, dla których brakuje definicji rozkładów
#'
#' @export
#'
distr_obj_get_missing_day_idxs <- function(distr_obj, start_point, day_idxs) {
    assert(is_distr_obj(distr_obj))

    missings <- vapply(day_idxs,
                       FUN = function(dix) {
                           distr_def <-
                               distr_obj_get_distr(distr_obj, start_point, day_ix = dix)
                           return(is.null(distr_def) # brak rozkładu
                                  || is.character(distr_def)) # jest tylko informacja o błędzie
                       },
                       FUN.VALUE = logical(1))
    return(day_idxs[missings])
}

#'
#' Weryfikuje, czy obiekt jest klasy distr_obj.
#'
#' @param obj obiekt do zweryfikowania
#'
#' @return TRUE, jeśli jest klasy distr_obj
#'
#' @export
#'
is_distr_obj <- function(obj) {
    return(!is.null(obj) && class(obj) == "distr_obj")
}
