#
# PSE S.A.
# 2018
#
# Funkcje związane z konwersją i walidacją dat.
#


#'
#' Reprezentacja tekstowa wektora dat.
#'
#' Funkcja pomocnicza do tworzenia tekstowej reprezentacji dat/przedziałów dat.
#'
#' @param d wektor dat w formacie YYYYMMDD - typ numeric (integer)
#' @param lim maksymalna długość wektora dat reprezentowana tekstowo w całości
#'
#' @return reprezentacja dat (character)
#'
#' @export
#'
dates_string <- function(d, lim = 3) {
    # dużo dat
    if (length(d) > lim) {
        dd <- paste0(print_date(d[1]), ", ", print_date(d[2]), ", ... , ",
                     print_date(d[length(d)]))
    } else {
        dd <- NULL
        for (i in seq_along(d))
            dd <- paste0(dd, print_date(d[i]), ifelse(i == length(d), "", ", "))
    }
    return(dd)
}


#'
#' Konwersja daty z integer do formatu character.
#'
#' @param d data w postaci integera (YYYYMMDD)
#' @param sep znak separatora między komponentami daty
#'
#' @return data w odpowiednim formacie (character)
#'
#' @export
#'
print_date <- function(d, sep = "/") {
    y <- d %/% 10 ^ 4
    m <- (d %% 10 ^ 4) %/% 10 ^ 2
    d <- d %% 10 ^ 2
    if (d < 10) d <- paste0("0", d)
    if (m < 10) m <- paste0("0", m)
    return(paste(y, m, d, sep = sep))
}


#'
#' Walidacja dat w formacie tekstowym.
#'
#' @param d wektor dat (character)
#'
#' @return wartość logiczna: TRUE, jeśli daty są w dobrym formacie,
#' FALSE w przeciwnym wypadku
#'
#' @export
#'
valid_date <- function(d) {
    d <- as.numeric(unlist(strsplit(x = gsub("-|/", "", d), split = " ")))
    if (any(is.na(d))) return(FALSE)
    if (length(d) > 2) return(FALSE)
    if (d[1] < 20000101 | d[1] > 22000101) return(FALSE)
    if (length(d) == 2) {
        if (d[2] > 2400 | d[2] < 0) return(FALSE)
    }
    return(TRUE)
}

#'
#' Tworzy data.frame zawierający ciąg timestampów z oznaczeniem punktu startowego
#' i indeksu dnia.
#'
#' @param from_date bazowa data w formacie YYYYMMDD (typ: integer)
#' @param horizon horyzont w dniach
#' @param start_point punkt startowy danych (np. 0, 900, typ: integer)
#' @param start_from_hour godzina rozpoczęcia ciągu timestampów (np. 0, 900, typ: integer)
#'
#' @return data.frame z następującymi kolumnami
#' \describe{
#'   \item{ts}{Timestamp w formacie YYYY-MM-DD HHMM}
#'   \item{punkt_startowy}{Podany punkt startowy}
#'   \item{data}{Dzień w formacie YYYYMMDD}
#'   \item{dzien_ix}{Indeks dnia w ramce}
#' }
#'
#' @export
#'
build_date_seq_df <- function(from_date, horizon, start_point, start_from_hour = 0) {
    fdate <- as.Date(int2ts(from_date))
    tdate <- seq(fdate, length.out = 2, by = sprintf("%s days", horizon - 1))[2]

    fts <- as.POSIXlt(paste(fdate, sprintf("%02d:15", start_from_hour / 100)))
    tts <- as.POSIXlt(paste(tdate, "24:00"))
    exp_ts <- seq(from = fts, to = tts, by = "15 min")
    exp_ts_str <- format(exp_ts, "%Y-%m-%d %H%M")

    fix_ts <- which(endsWith(exp_ts_str, " 0000"))
    exp_ts_str[fix_ts] <- vapply(exp_ts[fix_ts],
                                 function(ts) paste(seq(ts,
                                                        length.out = 2,
                                                        by = "-1 day")[2], "2400"),
                                 FUN.VALUE = character(1))

    res <- data.frame(ts = exp_ts_str,
                      punkt_startowy = start_point,
                      stringsAsFactors = FALSE)

    # wstrzyknięcie indeksu dni
    res$data <- gsub("^([^ ]+) \\d{4}$", "\\1", exp_ts_str)
    dates <- sort(unique(res$data))
    date_seq_df <- data.frame(data = dates, dzien_ix = seq_along(dates) - 1,
                              stringsAsFactors = FALSE)

    res <- merge(res, date_seq_df, by = "data")
    res$data <- ts2int(res$data)
    return(res[, c("ts", "punkt_startowy", "data", "dzien_ix")])
}
