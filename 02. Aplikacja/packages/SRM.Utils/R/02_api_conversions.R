#
# PSE S.A.
# 2018
#
# Konwersje typów uzywane w SRM.
#

#
#' Transformacja indeksów czasowych dni (integer na character).
#'
#' @param int wektor dat w formacie YYYYMMDD (integer/numeric)
#'
#' @return wektor dat w formacie "YYYY-MM-DD" (character)
#'
#' @export
#
int2ts <- function(int) {

    s1 <- c(1, 5, 7)
    s2 <- c(4, 6, 8)
    fun <- function(x) paste0(substring(x, s1, s2), collapse = "-")
    res <- vapply(int, FUN = fun, FUN.VALUE = "")
    return(res)
}

#
#' Transformacja indeksów czasowych dni (integer na listę integerów).
#'
#' @param int wektor dat w formacie YYYYMMDD (integer/numeric)
#'
#' @return lista elementów dat: y(ear), m(onth), d(ay) (integer)
#'
#' @export
#
int2intlist <- function(int) {
    s1 <- c(1, 5, 7)
    s2 <- c(4, 6, 8)
    fun <- function(x) {
        si <- as.integer(substring(x, s1, s2))
        list(y = si[1], m = si[2], d = si[3])
        }
    res <- lapply(int, FUN = fun)
    return(res)
}

#
#' Transformacja indeksów czasowych w formacie characterowym na indeksy
#' dni w formacie integerowym.
#'
#' @param ts wektor dat w formacie "YYYY-MM-DD" lub
#'           "YYYY-MM-DD xxxx" (typ character)
#'
#' @return wektor dat w formacie YYYYMMDD (integer/numeric)
#'
#' @export
#
ts2int <- function(ts) {

    d <- substr(ts, 1, 10)
    d <- gsub("-", "", d)

    res <- as.numeric(d)
    return(res)
}

#'
#' Formatuje obiekt clasy timediff do formatu Xh Xm X.XXX.s.
#'
#' @param ts_diff object do sformatowania
#'
#' @return text reprezentujacy sformatowany obiekt
#'
#' @export
#'
fmt_timediff <- function(ts_diff) {
    stopifnot(class(ts_diff) == "difftime")
    ds <- as.numeric(ts_diff, units = "secs")

    parts <- c()
    if (ds > 3600) {
        dh <- ds %/% 3600
        parts <- c(parts, paste0(dh, "h"))
        ds <- ds - dh * 3600
    }
    if (ds > 60) {
        dm <- ds %/% 60
        parts <- c(parts, paste0(dm, "m"))
        ds <- ds - dm * 60
    }

    parts <- c(parts, paste0(round(ds, digits = 3), "s"))
    return(paste0(parts, collapse = " "))
}

#'
#' Formatuje wartość puntu początkowego do postaci HH:MM.
#'
#' @param start_point punkty startowe do sformatorwania (integer(N))
#'
#' @return wektor ze sformatowanymi punktami startowymi
#'
#' @export
#'
start_point2str <- function(start_point) {
    spoint_strs <- sprintf("%04d", start_point)
    res <- paste0(substring(spoint_strs, 1, 2), ":", substring(spoint_strs, 3, 4))
    return(res)
}

#'
#' Funkcja odwrotna do start_point2str: konwertuje sformatowaną postać start_point
#' do postaci oryginalnej.
#'
#' @param start_point_strs sformatowane stringi (character(N))
#'
#' @return odtworzone start points (integer(N))
#'
#' @export
#'
str2start_point <- function(start_point_strs) {
    stopifnot(is.character(start_point_strs))

    res <- as.integer(gsub(":", "", start_point_strs))
    return(res)
}
