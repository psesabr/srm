#
# PSE S.A.
# 2018
#
# Funkcje implementujące widgety interfejsu użytkownika
#


#'
#' Funkcja inicjuje widget daty (combo box).
#'
#' Funkcja pomocnicza odpowiedzialna za stworzenie i obsługę widgeta wyboru daty.
#'
#' @param tile tytuł/polecenie dla widgetu
#' @param dates data.frame dostępnych dat, o strukturze 3 kolumn:
#'        1wsza: rok (yyyy - integer)
#'        2ga: miesiąc (mm - integer)
#'        3cia: dzień (dd - integer)
#'
#' @return reprezentacja dat
#'
#' @keywords internal
#' @noRd
#'
init_date <- function(title, dates) {

    # layout
    gh <- ggroup(horizontal = TRUE)
    fr <- gframe(text = title, spacing = gui$COMMON_SP)
    gv1 <- ggroup(horizontal = FALSE)
    gv2 <- ggroup(horizontal = FALSE)
    gv3 <- ggroup(horizontal = FALSE)
    addSpace(gh, gui$COMMON_SP_SECTIONS * 2)
    add(gh, gv1)
    addSpace(gh, gui$COMMON_SP_SECTIONS)
    add(gh, gv2)
    addSpace(gh, gui$COMMON_SP_SECTIONS)
    add(gh, gv3)
    envir <- environment()
    add(fr, gh, expand = TRUE, fill = TRUE)
    fr$set_size(gui$COMMON_DATE_SIZE)

    # combobox YYYY
    add(gv1, glabel(gui$COMMON_DATE_Y, editable = FALSE))
    yyyy <- sort(unique(dates$yyyy))
    cby <- gcombobox(yyyy, selected = 0, handler =
                         # wybor roku
                         function(h, ...) {
                             yi <- svalue(h[[1]])
                             cbm <- get("cbm", envir = envir)
                             gv2 <- get("gv2", envir = envir)
                             gv3 <- get("gv3", envir = envir)
                             visible(gv2) <- FALSE
                             visible(gv3) <- FALSE
                             dates <- get("dates", envir = envir)
                             ii <- which(dates$yyyy == yi)
                             if (length(ii) > 0) {
                                 mm <- sort(unique(dates$mm[ii]))
                                 cbm$set_items(value = mm)
                                 visible(gv2) <- TRUE
                             }
                         })
    cby$set_size(gui$COMMON_DATE_FIELD_SIZE1)
    add(gv1, cby)

    # combobox MM
    add(gv2, glabel(gui$COMMON_DATE_M, editable = FALSE))
    cbm <- gcombobox(c(1), handler =
                         # wybor mc
                         function(h, ...) {
                             cby <- get("cby", envir = envir)
                             cbd <- get("cbd", envir = envir)
                             dates <- get("dates", envir = envir)
                             yi <- svalue(cby)
                             mi <- svalue(h[[1]])
                             gv3 <- get("gv3", envir = envir)
                             visible(gv3) <- FALSE
                             ii1 <- which(dates$yyyy == yi)
                             ii2 <- which(dates$mm == mi)
                             ii <- intersect(ii1, ii2)
                             if (length(ii) > 0) {
                                 dd <- sort(unique(dates$dd[ii]))
                                 cbd$set_items(value = dd)
                                 visible(gv3) <- TRUE
                             }
                         })
    cbm$set_size(gui$COMMON_DATE_FIELD_SIZE2)
    add(gv2, cbm)
    visible(gv2) <- FALSE

    # combobox DD
    add(gv3, glabel(gui$COMMON_DATE_D, editable = FALSE))
    cbd <- gcombobox(c(1), handler =
                         # wybor dnia
                         function(h, ...) {
                         })
    cbd$set_size(gui$COMMON_DATE_FIELD_SIZE2)
    add(gv3, cbd)
    visible(gv3) <- FALSE

    # return
    invisible(list(obj = fr, y = cby, m = cbm, d = cbd, group = gh))
}


#'
#' Przycisk OK.
#'
#' Funkcja tworząca przycisk (button) OK.
#'
#' @param w okno główne, które zamykać będzie przycisk
#' @param lab tekst informacji
#' @param ico ikona na przycisku
#'
#' @return utworzony przycisk (gbutton)
#'
#' @keywords internal
#' @noRd
#'
ok_button <- function(w, h = NULL,
                      lab = gui$COMMON_OK,
                      ico = gui$COMMON_OK_ICO) {
    # domyślna akcja
    if (is.null(h)) {
        h <- function(h, ...) {
            dispose(w)
        }
    }
    # definicja przycisku
    ok_act <- gaction(
        label = lab,
        icon =  ico,
        handler = h)

    # przycisk : inicjowanie
    ok_btn <- gbutton(action = ok_act)
    return(ok_btn)
}


#'
#' Przycisk Anuluj.
#'
#' Funkcja tworzaca przycisk (button) Anuluj.
#'
#' @param w okno główne, które zamykać będzie przycisk
#' @param lab tekst informacji
#' @param ico ikona na przycisku
#'
#' @return utworzony przycisk (gbutton)
#'
#' @keywords internal
#' @noRd
#'
cancel_button <- function(w, h = NULL,
                          lab = gui$COMMON_CANCEL,
                          ico = gui$COMMON_CANCEL_ICO) {
    # domyslny akcja
    if (is.null(h)) {
        h <- function(h, ...) {
            dispose(w)
        }
    }
    # definicja przycisku
    cancel_act <- gaction(
        label = lab,
        icon = ico,
        handler = h)

    # przycisk : inicjowanie
    cancel_btn <- gbutton(action = cancel_act)
    return(cancel_btn)
}


#'
#' Okno/przestrzeń tabeli z wynikami.
#'
#' @param df data.frame z danymi
#' @param w okno główne
#' @param ok_btn logiczna wartość, czy wstawić przycisk OK
#' @param ts opcjonalny wektor znaczników czasowych
#'
#' @return  NULL
#'
#' @keywords internal
#' @noRd
#'
init_table <- function(df, w = NULL, ok_btn = TRUE, ts = NULL) {

    # okno tabeli
    if (is.null(w)) {
        w <- gwindow(title = gui$MENU_TITLE, visible = FALSE,
                     width = 600, height = 500, parent = gui$COMMON_WINDOW_POSITION)
    }

    # dolaczanie time stampa
    if (!is.null(ts)) df <- cbind(ts, df)

    # tworzenie tabeli + uklad
    tab <- gtable(items = df)
    g <- ggroup(horizontal = FALSE)
    add(g, add_spacing(tab, gui$COMMON_SP), expand = TRUE, fill = TRUE)
    g2 <- ggroup(horizontal = FALSE)
    add(g2, g, expand = TRUE, fill = TRUE)

    # przycisk ok
    if (ok_btn) {
        okbtn <- ok_button(w = w)
        g3 <- ggroup(horizontal = TRUE)
        addSpring(g3)
        okbtn <- ok_button(w = w)
        add(g3, add_spacing(okbtn, gui$COMMON_SP_SECTIONS))
        add(g2, g3)
    }

    # wyswietlenie
    add(w, g2, expand = TRUE, fill = TRUE)
    visible(w) <- TRUE
    invisible(NULL)
}


#'
#' Przycisk informacyjny.
#'
#' Funkcja tworzaca przycisk button informacyjny.
#'
#' @param txt tekst informacji
#' @param lab okno główne
#' @param ico ikona na przycisku
#'
#' @return utworzony przycisk (gbutton) wraz z wolną przestrzenią
#'
#' @keywords internal
#' @noRd
#'
info_button <- function(txt, lab = "info", h = NULL, sp = gui$COMMON_SP,
                        getbtn = FALSE) {
    if (is.null(h)) {
        h <- function(h, ...) {
            msg_info(txt, type = "info", parent = gui$COMMON_SMALL_WINDOW_POSITION)
        }
    }
    act <- gaction(label = lab, icon = gui$COMMON_INFO_ICO, handler = h)
    btn <- gbutton(action = act)
    if (getbtn) return(btn)
    return(add_spacing(btn, sp))
}


#'
#' Okno wiadomości.
#'
#' @param msg tekst wiadomości
#' @param title tytuł okna wiadomości
#' @param type typ wiadomości:
#'        \item{info}{informacyjna}
#'        \item{warn}{ostrzegawcza}
#'        \item{err}{błąd}
#' @param parent (opcjonalnie) pozycja okna
#' @param ok wartość logiczna, czy wstawić przycisk OK?
#' @param h (opcjonalnie) funkcja obsługi dodatkowego przycisku OK
#' @param args (opcjonalnie) argumenty dodatkowe do funkcji obsługi dodatkowego
#'        przycisku OK
#'
#' @return lista z 2 elementami:
#'         w - okno głowne wiadomości
#'         gv - komponent (ggroup) zawierający wnętrze okna wiadomości
#'
#' @keywords internal
#' @noRd
#'
msg_info <- function(msg, title = "", type = "info",
                     parent = gui$COMMON_SMALL_WINDOW_POSITION,
                     ok = TRUE, h = NULL, args = NULL) {
    icons <- list()
    icons[["info"]] <- gui$COMMON_INFO_PATH
    icons[["warn"]] <- gui$COMMON_WARN_PATH
    icons[["err"]] <- gui$COMMON_ERR_PATH
    w <- gwindow(title = title, width = 300, height = 100, parent = parent)
    lab <- glabel(text = msg)
    gv <- ggroup(horizontal = FALSE, spacing = 0)
    gh1 <- ggroup(horizontal = TRUE, spacing = 0)
    gh2 <- ggroup(horizontal = TRUE, spacing = gui$COMMON_SP)
    img <- gimage(filename = icons[[type]])
    add(gh1, add_spacing(img, gui$COMMON_SP))
    add(gh1, add_spacing(lab, gui$COMMON_SP))
    addSpace(gh1, gui$COMMON_SP)
    addSpring(gh2)
    if (ok) {
        # obsługa ok
        if (!is.null(h)) {
            hok <- function(han, ...) {
                dispose(w)
                do.call(h, args)
            }
        } else {
            hok <- NULL
        }
        add(gh2, add_spacing(ok_button(w, h = hok, ico = NULL,
                                       lab = paste("  ", gui$COMMON_OK, "  ")),
                             gui$COMMON_SP))
    }
    add(gv, gh1)
    add(gv, gh2)
    add(w, gv)
    visible(w) <- TRUE
    focus(w) <- TRUE
    invisible(list(w = w, gv = gv))
}


#'
#' Okno pytania/potwierdzenia.
#'
#' @param msg tekst pytania
#' @param title tytuł okna
#'
#' @return lista z 2 elementami:
#'         w - okno główne pytania
#'         y - komponent (gbutton) reprezentujący przycisk TAK, znajdujący się
#'             w oknie potwierdzenia
#'         n - komponent (gbutton) reprezentujący przycisk NIE, znajdujący się
#'             w oknie potwierdzenia
#'
#' @keywords internal
#' @noRd
#'
dialog_yes_no <- function(msg, title = "") {
    icon <- gui$COMMON_Q_PATH
    w <- gwindow(title = title, width = 300, height = 1,
                 parent = gui$COMMON_SMALL_WINDOW_POSITION)
    lab <- glabel(text = msg)
    gv <- ggroup(horizontal = FALSE, spacing = 0)
    gh1 <- ggroup(horizontal = TRUE, spacing = 0)
    gh2 <- ggroup(horizontal = TRUE, spacing = gui$COMMON_SP)
    img <- gimage(filename = icon)
    add(gh1, add_spacing(img, gui$COMMON_SP))
    add(gh1, add_spacing(lab, gui$COMMON_SP))
    addSpace(gh1, gui$COMMON_SP)
    addSpring(gh2)
    yes_act <- gaction(label = paste0(" ", gui$COMMON_YES, " "),
                       icon = gui$COMMON_YES_ICO)
    no_act <- gaction(label = paste0(" ", gui$COMMON_NO, " "), icon = gui$COMMON_NO_ICO,
                      handler = function(h, ...) dispose(w))
    yes_btn <- gbutton(action = yes_act)
    no_btn <- gbutton(action = no_act)
    add(gh2, add_spacing(no_btn, gui$COMMON_SP))
    add(gh2, add_spacing(yes_btn, gui$COMMON_SP))
    add(gv, gh1)
    add(gv, gh2)
    add(w, gv)
    visible(w) <- TRUE
    focus(w) <- TRUE
    # zwraca okno z przyciskami
    invisible(list(w = w, y = yes_btn, n = no_btn))
}


#'
#' Tworzy widget progresu.
#'
#' @param w (opcjonalny) okno, w którym pojawi się widget, domyślnie tworzone nowe okno
#'
#' @return lista z elementami:
#'         p - okno progresu
#'         h - funkcja do obsługi paska progresu
#'
#' @keywords internal
#' @noRd
#'
progressbar <- function(w = NULL) {

    if (is.null(w)) {
        w <- gwindow(title = "",
                     width = gui$COMMON_PROGRESSBAR_SIZE[1],
                     height = gui$COMMON_PROGRESSBAR_SIZE[2],
                     parent = gui$COMMON_SMALL_WINDOW_POSITION)
    }
    envir <- environment()
    gv <- ggroup(horizontal = FALSE)
    gh1 <- ggroup(horizontal = TRUE, spacing = gui$COMMON_SP)
    gh2 <- ggroup(horizontal = TRUE)
    p <- gprogressbar(value = 0)
    l0 <- glabel(text = "Wykonano:", editable = FALSE)
    l <- glabel(text = "0%", editable = FALSE)
    add(gh1, l0)
    add(gh1, l)
    add(gh2, p, fill = TRUE, expand = TRUE)
    add(gv, gh1)
    add(gv, gh2, expand = TRUE, fill = TRUE)
    add(w, add_spacing(gv, gui$COMMON_SP))
    focus(w) <- TRUE

    # zmiana wartosci
    change_val <- function(x) {
        w <- get("w", envir = envir)
        p <- get("p", envir = envir)
        l <- get("l", envir = envir)
        if (x < 0) {
            x <- svalue(p) - x
        }
        if (x >= 100) {
            x <- 100
            svalue(p) <- x
            svalue(l) <- paste0(x, "%")
            tryCatch(expr = dispose(w))
            invisible(return(NULL))
        }
        svalue(p) <- x
        svalue(l) <- paste0(x, "%")
        focus(w) <- TRUE
    }
    return(list(p = w, h = change_val))
}


#'
#' Tworzy widget progresu z opcją kilku etapów.
#'
#' @param w (opcjonalny) okno w którym pojawi się widget, domyślnie tworzone nowe okno
#'
#' @return lista z elementami:
#'         p - okno progresu
#'         h - funkcja do obsługi paska progresu
#'
#' @keywords internal
#' @noRd
#'
progressbar2 <- function(w = NULL) {

    if (is.null(w)) {
        w <- gwindow(title = "",
                     width = gui$COMMON_PROGRESSBAR_SIZE[1] * 1.2,
                     height = gui$COMMON_PROGRESSBAR_SIZE[2] * 1.3,
                     parent = gui$COMMON_SMALL_WINDOW_POSITION)
    }
    envir <- environment()
    gv <- ggroup(horizontal = FALSE)
    gh0 <- ggroup(horizontal = TRUE)
    gh1 <- ggroup(horizontal = TRUE, spacing = gui$COMMON_SP)
    gh2 <- ggroup(horizontal = TRUE)
    p <- gprogressbar(value = 0)
    step_l <- glabel(text = "", editable = FALSE)
    step_l2 <- glabel(text = "", editable = FALSE)
    font(step_l) <- list(weight = "bold")
    l0 <- glabel(text = "Wykonano:", editable = FALSE)
    l <- glabel(text = "0%", editable = FALSE)
    add(gh0, step_l)
    add(gh0, step_l2)
    add(gh1, l0)
    add(gh1, l)
    add(gh2, p, fill = TRUE, expand = TRUE)
    add(gv, gh0)
    add(gv, gh1)
    add(gv, gh2, expand = TRUE, fill = TRUE)
    add(w, add_spacing(gv, gui$COMMON_SP))
    focus(w) <- TRUE

    # zmiana wartosci
    change_val <- function(x, lab = NULL, lab2 = NULL) {
        w <- get("w", envir = envir)
        p <- get("p", envir = envir)
        l <- get("l", envir = envir)
        step_l <- get("step_l", envir = envir)
        step_l2 <- get("step_l2", envir = envir)
        if (x > 100) {
            x <- 100
            svalue(p) <- x
            svalue(l) <- paste0(x, "%")
            tryCatch(expr = dispose(w))
            invisible(return(NULL))
        }
        svalue(p) <- x
        svalue(l) <- paste0(x, "%")
        if (!is.null(lab)) svalue(step_l) <- lab
        if (!is.null(lab2)) svalue(step_l2) <- paste0("(", lab2, ")")
        focus(w) <- TRUE
    }
    return(list(p = w, h = change_val))
}


#'
#' Okno info o aplikacji.
#'
#' @return NULL
#'
#' @keywords internal
#'
#' @keywords internal
#' @noRd
#'
about_app <- function() {

    w <- gwindow(parent = gui$COMMON_SMALL_WINDOW_POSITION, title = gui$MENU_TAB5_APP_INFO)
    gv <- ggroup(horizontal = FALSE, container = w)
    gh <- ggroup(horizontal = TRUE)
    gh0 <- ggroup(horizontal = TRUE)
    img <- gimage(gui$COMMON_ABOUT_IMG)
    lab <- glabel(gui$COMMON_ABOUT)
    font(lab) <- list(size = 11)
    add(gv, add_spacing(lab, gui$COMMON_SP))
    add(gh0, add_spacing(img, gui$COMMON_SP), anchor = -1)
    add(gv, gh0)
    addSpring(gh)
    add(gh, add_spacing(ok_button(w = w), gui$COMMON_SP))
    add(gv, gh)

    invisible(NULL)
}


#'
#' Animacja oczekiwania na wynik.
#'
#' @param lab tekst informacyjny podczas oczekiwania
#'
#' @return lista 2 elementów:
#'          w - okno oczekiwania (gwindow)
#'          l - napis (glabel)
#'
#' @keywords internal
#' @noRd
#'
waiting <- function(lab) {

    w <- gwindow(title = "", width = 5, height = 5,
                 parent = gui$COMMON_SMALL_WINDOW_POSITION)
    gv <- ggroup(horizontal = FALSE, spacing = 0, container = w)
    l <- glabel(text = lab, editable = FALSE)
    img <- gimage(filename = gui$COMMON_WAIT_PATH)
    add(gv, add_spacing(l, gui$COMMON_SP))
    add(gv, add_spacing(img, spacing = gui$COMMON_SP_SECTIONS))
    add(w, gv)
    focus(w) <- TRUE
    Sys.sleep(0.5)

    invisible(return(list(w = w, l = l)))
}


#'
#' Dialog zapisu pliku.
#'
#' @param title tytuł okna dialogu
#' @param init_filename domyślna nazwa pliku
#' @param init_dir domyślny katalog zapisu
#'
#' @return NULL
#'
#' @keywords internal
#' @noRd
#'
save_dialog <- function(title = "", init_filename = "", init_dir = "/") {

    actiontype <- GtkFileChooserAction["save"]
    button_with_id <- list(
        "ok" = c("gtk-ok", GtkResponseType["ok"]),
        "cancel" = c("gtk-cancel", GtkResponseType["cancel"])
    )
    which_buttons <- c("ok", "cancel")
    okhandler_default <- function(h, ...) {
        svalue(h$action) <- h$file
        do.call(h$action, list(h$file))
    }
    cancelhandler <- function(h, ...) {
        dispose(h$obj)
        return(NA)
    }

    filechooser <- gtkFileChooserDialogNew(title = title, action = actiontype)
    gtkFileChooserSetCurrentName(filechooser, init_filename)
    gtkFileChooserSetCurrentFolderUri(filechooser, init_dir)

    for (i in which_buttons) {
        filechooser$AddButton(button_with_id[[i]][1], button_with_id[[i]][2])
    }

    resp <- filechooser$Run()

    file <- unlist(filechooser$GetFilenames())
    if (is.null(file)) file <- character(0)
    Encoding(file) <- "UTF-8"

    if (resp == GtkResponseType["cancel"]) {
        filechooser$Destroy()
        return(character(0))
    }
    if (resp == GtkResponseType["ok"]) {
        filechooser$Destroy()
        return(file)
    }

    filechooser$Destroy()
    return(character(0))
}
