#
# PSE S.A.
# 2018
#
# Funkcje do weryfikacji poprawności tabel z danymi
#

#'
#' Sprawdzenie zgodności indeksów czasowych w obiektach data.frame.
#'
#' @param  df1 obiekt data.frame
#' @param  df2 drugi obiekt data.frame
#' @param  ... kolejne obiekty data.frame
#'
#' @return wartość logiczna TRUE, jeśli indeksy czasowe są zgodne
#'
#' @keywords internal
#' @noRd
#'
check_df_ts <- function(df1, df2, ...) {

    df_list <- list(df1, df2, ...)
    n <- c()
    ts <- list()
    for (i in seq_along(df_list)) {
        ts[[i]] <- df_list[[i]][, "ts", drop = FALSE]
        n <- c(n, nrow(ts[[i]]))
    }
    if (any(n != n[1])) stop(srm_err("DATA_ERR_INCONSISTENT_RECNO"))
    ts_df <- ts[[1]]
    for (i in 2:length(ts)) {
        ts_df <- cbind(ts_df, ts[[i]])
    }
    apply(ts_df, 1, FUN = function(x) {
        if (any(x != x[1]))
            stop(srm_err("DATA_ERR_INCONSISTENT_TS"))
    })
    return(TRUE)
}

#'
#' Walidacja pojedynczego data.frame'a - zmiennych oraz struktury
#' indeksów czasowych.
#'
#' @param  path ścieżka do pliku .csv
#' @param  nms wektor poprawnych nazw zmiennych
#' @param  ts wartość logiczna, TRUE jeśli indeksy czasowe występują
#'         wyłącznie w pierwszej kolumnie w formacie "YYYY-MM-DD xxxx",
#'         FALSE jeśli są podzielone na trzy kolumny (dzień, godzina
#'         oraz kwadrans)
#'
#' @keywords internal
#' @noRd
#'
check_df <- function(path, nms, ts = FALSE) {

    data <- read.csv2(file = path, stringsAsFactors = FALSE)
    vi <- ifelse(ts, 2, 4)
    m <- match(nms, colnames(data)[vi:ncol(data)])
    if (any(is.na(m))) {
        stop(srm_err("DATA_ERR_INVALID_VARS"))
    }

    e <- srm_err("DATA_ERR_INVALID_TSFMT")
    if (ts) {
        c1 <- unlist(strsplit(data[, 1], " "))
        d <- c1[seq(1, length(c1), 2)]
        q <- c1[seq(2, length(c1), 2)]
    } else {
        d <- substr(data[, 1], 1, 10)
        q <- data[, 3]
    }
    d2 <- as.numeric(gsub("-|/", "", d))
    if (any(is.na(d2))) stop(e)
    if (any(d2 < 20000101) | any(d2 > 22000101)) stop(e)
    q2 <- gsub("A", "", q)
    q2[which(nchar(q2) == 2)] <- paste0("00", q2[which(nchar(q2) == 2)])
    q2[which(nchar(q2) == 3)] <- paste0("0", q2[which(nchar(q2) == 3)])
    m1 <- match(as.numeric(substr(q2, 1, 2)), 0:24)
    if (any(is.na(m1))) stop(e)
    m2 <- match(as.numeric(substr(q2, 3, 4)), seq(0, 45, 15))
    if (any(is.na(m2))) stop(e)

    if (ts) {
        dh <- data[, 1]
        data <- data[, -1]
    } else {
        q3 <- as.character(q)
        q3[which(nchar(q3) == 2)] <- paste0("00", q3[which(nchar(q3) == 2)])
        q3[which(nchar(q3) == 3)] <- paste0("0", q3[which(nchar(q3) == 3)])
        d <- gsub("/", "-", d)
        dh <- paste(d, q3)
        data <- data[, -c(1:3)]
    }
    ord <- order(dh)
    data2 <- data[ord, ]
    data2 <- apply(data2, 2, as.numeric)
    data3 <- data.frame(ts = dh[ord], data2, stringsAsFactors = FALSE)

    rs <- rowSums(data3[, -1])
    ind <- which(rs == 0)
    if (length(ind) > 0) data3 <- data3[-ind, ]
    return(data3)
}

#'
#' Sprawdza poprawność zakresu danych planu-historii.
#'
#' Rzuca ostrzeżenia, jeśli wykryje nieprawidłowości.
#'
#' @param df tabela z danymi do zweryfikowania
#'
#' @return poprawiona tabela (obcięta do poprawnego zakresu danych)
#'
#' @keywords internal
#' @noRd
#'
check_plan_hist_df_range <- function(df) {
    if (is.null(df)) return(NULL)
    stopifnot("dzien_ix" %in% colnames(df))

    # Only starting at the beginning of the day
    start_date <- unlist(strsplit(df[1, "ts"], " "))[[1]]
    if (df[1, "ts"] > paste(start_date, "0015")) {
        start_date <- as.character(as.Date(start_date) + 1)
    }
    first_ts <- paste(start_date, "0015")

    to_skip_head <- nrow(df[df$ts < first_ts, ])
    if (to_skip_head > 0) {
        warning(rpl(gui$DATA_WARN_SKIPPED_FIRST, x = to_skip_head, y = first_ts))
    }
    df <- df[df$ts >= first_ts, ]

    end_date <- unlist(strsplit(df[nrow(df), "ts"], " "))[[1]]
    last_ts <- paste(end_date, "2400")
    if (df[nrow(df), ]$ts < last_ts) { # check if last date is complete
        last_ts <- paste(format(as.Date(end_date) -1, "%Y-%m-%d"), "2400")
    }

    to_skip_tail <- nrow(df[df$ts > last_ts, ])
    if (to_skip_tail > 0) {
        warning(rpl(gui$DATA_WARN_SKIPPED_LAST, x = to_skip_tail, y = last_ts))
    }
    df <- df[df$ts <= last_ts, ]

    # weryfikacja spójności indeksowania dni
    unexected_day_ix <- setdiff(unique(df[, "dzien_ix"]), 0:2)
    if (length(unexected_day_ix) > 0) {
        stop(srm_err("DATA_ERR_UNEXPECTED_DAY_IX", x = paste(unexected_day_ix, collapse = ", ")))
    }

    return(df)
}


#'
#' Sprawdza poprawność zakresu danych planu-prognozy (maksymalnie na 2 dni do przodu).
#'
#' Rzuca ostrzeżenia, jeśli wykryje nieprawidłowości.
#'
#' @param df tabela z danymi do zweryfikowania
#'
#' @return poprawiona tabela (obcięta do poprawnego zakresu danych)
#'
#' @keywords internal
#' @noRd
#'
check_plan_forecast_df_range <- function(df) {
    if (is.null(df)) return(NULL)

    # Only starting at the beginning of the day
    start_date <- unlist(strsplit(df[1, "ts"], " "))[[1]]
    if (df[1, "ts"] > paste(start_date, "0015")) {
        start_date <- as.character(as.Date(start_date) + 1)
    }
    first_ts <- paste(start_date, "0015")

    to_skip_head <- nrow(df[df$ts < first_ts, ])
    if (to_skip_head > 0) {
        warning(rpl(gui$DATA_WARN_SKIPPED_FIRST, x = to_skip_head, y = first_ts))
    }
    df <- df[df$ts >= first_ts, ]

    # Plans can extend no further than 2 days into the future
    end_date <- unlist(strsplit(df[nrow(df), "ts"], " "))[[1]]
    if (as.Date(end_date) > as.Date(start_date) + 2) {
        end_date <- format(as.Date(start_date) + 2, "%Y-%m-%d")
    }
    last_ts <- paste(end_date, "2400")
    if (df[nrow(df), ]$ts < last_ts) { # check if last date is complete
        last_ts <- paste(format(as.Date(end_date) -1, "%Y-%m-%d"), "2400")
    }

    to_skip_tail <- nrow(df[df$ts > last_ts, ])
    if (to_skip_tail > 0) {
        warning(rpl(gui$DATA_WARN_SKIPPED_LAST, x = to_skip_tail, y = last_ts))
    }
    df <- df[df$ts <= last_ts, ]

    return(df)
}

#'
#' Dodaje kolumnę dzien_ix z indeksem dnia od poczatku ramki planu.
#'
#' @param plan tabela z danymi planu do uzupełnienia o dzien_ix
#'
#' @return tabela z nową kolumną
#'
#' @keywords internal
#' @noRd
#'
inpute_date_ix <- function(plan) {
    plan$date <- ts2int(plan$ts)

    dates <- sort(unique(plan$date))
    date_seq_df <- data.frame(date = dates, dzien_ix = seq_along(dates) - 1)

    df <- merge(x = plan, y = date_seq_df, by = "date")
    df$date <- NULL # usuwamy kolumne date
    return(df)
}
