#
# PSE S.A.
# 2018
#
# Plik walidacyjny - kopiowanie bazy i danych
#

#'
#' Wykonuje zbieranie danych referencyjnych (db i Rdata) do wykonanego raportu.
#'
#' @param path ścieżki
#' @param out_dir folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return lista nazwana
#'
#' @keywords internal
#' @noRd
#'
validation_rd_db <- function(paths, out_dir) {
    stopifnot(dir.exists(out_dir))

    #
    # Tworzenie folderów
    #
    pkg_loginfo(msgs$RD_DB_COPY_DB_DIR_CREATE)
    folder_db <- file.path(out_dir, "db")
    dir.create(folder_db, recursive = TRUE, showWarnings = FALSE)

    pkg_loginfo(msgs$RD_DB_COPY_DB_COPY)
    file.copy(from = paths$db, to = file.path(folder_db, basename(paths$db)),
              overwrite = TRUE)

    pkg_loginfo(msgs$RD_DB_COPY_RD_DIR_CREATE)
    folder_rd <- file.path(out_dir, "rd")
    dir.create(folder_rd, recursive = TRUE, showWarnings = FALSE)

    pkg_loginfo(msgs$RD_DB_COPY_RD_COPY)
    file.copy(from = paths$rd, to = file.path(folder_rd, basename(paths$rd)),
              overwrite = TRUE)

    return(list())
}

#'
#' Symulacja zbierania danych referencyjnych (db i Rdata) do wykonanego raportu.
#'
#' @param out_dir folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return lista nazwana
#'
#' @keywords internal
#' @noRd
#'
validation_rd_db_dummy <- function(out_dir) {
    stopifnot(dir.exists(out_dir))
    pkg_loginfo(msgs$RD_DB_COPY_DB_DIR_CREATE)
    pkg_loginfo(msgs$RD_DB_COPY_DB_COPY)
    pkg_loginfo(msgs$RD_DB_COPY_RD_DIR_CREATE)
    pkg_loginfo(msgs$RD_DB_COPY_RD_COPY)
    return(list())
}
