#
# PSE S.A.
# 2018
#
# Plik generujący raport z walidacji ana podstawie uzyskanych rezultatów
#

#'
#' Generuje texowe źródło raportu walidacyjnego.
#'
#' @param paths ustawienia ścieżek do plików konfiguracyjnych
#' @param config parametry konfiguracyjne (list nazwana)
#' @param phases_out wyniki poszczególnych faz validacji.
#' @param seed użyte ziarno losowe.
#' @param out_dir folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return ścieżka do utworzonego pliku tex
#'
#' @keywords internal
#' @noRd
#'
generate_latex <- function(paths, config, phases_out, seed, out_dir) {
    stopifnot(dir.exists(out_dir))

    pkg_loginfo(msgs$LATEX_CREATE_TEX)

    #
    # Zaczytanie wartości templatu
    #
    tex_templ_fpath <- system.file("extdata/walidacja_template.tex", package = "SRM.Validation")
    tex_content <- readLines(tex_templ_fpath, encoding = "UTF-8")
    tex_content <- paste0(tex_content, collapse = "\n")

    # Kopiowanie figures na potrzeby pdflatex
    fig_dir <- file.path(out_dir, "figures")
    dir.create(fig_dir, recursive = TRUE, showWarnings = FALSE)
    file.copy(from = list.files(system.file("extdata/figures", package = "SRM.Validation"),
                                full.names = TRUE),
              to = fig_dir,
              recursive = TRUE,
              overwrite = TRUE)

    #
    # Wstawienie parametrów
    #
    par_list <- list()
    par_list[["*PATHDB*"]] <- path2latex(paths$db) # bazy danych
    par_list[["*PATHRB*"]] <- path2latex(paths$rd) # pliku z rozkładami
    par_list[["*CUTOFFZ*"]] <- config$CUTOFF_Z # dane zapotrzebowania na moc w KSE
    par_list[["*CUTOFFW*"]] <- config$CUTOFF_W # dane generacji źródeł wiatrowych
    par_list[["*CUTOFFM*"]] <- config$CUTOFF_M # dane salda wymiany międzynarodowej
    par_list[["*CUTOFFJ*"]] <- config$CUTOFF_J # dane generacji źródeł JWCD
    par_list[["*CUTOFFN*"]] <- config$CUTOFF_N # dane generacji źródeł nJWCD

    # rozkład miary odchylenia mocy niezbędnej do zbilansowania systemu
    par_list[["*KAPPAY*"]] <- config$KAPPA_Y
    # rozkład miary odchylenia zapotrzebowania na moc w KSE
    par_list[["*KAPPAZ*"]] <- config$KAPPA_Z
    # rozkład miary odchylenia generacji źródeł wiatrowych
    par_list[["*KAPPAW*"]] <- config$KAPPA_W
    # rozkład miary odchylenia salda wymiany międzynarodowej
    par_list[["*KAPPAM*"]] <- config$KAPPA_M
    # rozkład miary odchylenia generacji źródeł JWCD
    par_list[["*KAPPAJ*"]] <- config$KAPPA_J
    # rozkład miary odchylenia generacji źródeł nJWCD
    par_list[["*KAPPAN*"]] <- config$KAPPA_N

    # próg dla statystyki Kołmogorowa-Smirnowa
    par_list[["*MAXDISTANCE*"]] <- config$MAX_DISTANCE
    par_list[["*MINSIZE*"]] <- config$MIN_SIZE # minimalna liczebność grup
    par_list[["*MCSAMPLE*"]] <- config$MC_SAMPLE # wielkość próby w symulacjach Monte-Carlo
    par_list[["*SEED*"]] <- seed # ziarno dla generatora pseudo-losowego
    for (i in seq_along(par_list)) {
        if (is.na(par_list[[i]])) par_list[[i]] <- "NA"
        tex_content <- gsub(pattern = names(par_list)[i], replacement = par_list[[i]],
                            x = tex_content, fixed = TRUE)
    }

    #
    # Parametry etap 1.1
    #
    tex_content <- gsub(pattern = "*RANGE*", replacement = phases_out$RANGE, x = tex_content,
                        fixed = TRUE)
    tex_content <- gsub(pattern = "*STEPI*", replacement = phases_out$STEP_I, x = tex_content,
                        fixed = TRUE)


    #
    # Parametr etap 1.3
    #
    tex_content <- gsub(pattern = "*QUANTILECOLOR*", replacement = phases_out$QUANTILE_COLOR,
                        x = tex_content, fixed = TRUE)


    #
    # Etap 1.3
    #
    res13_list <- phases_out$res13_list
    tex_content <- gsub(pattern = "*SMM13*",
                        replacement = graph2latex_sm(res13_list$M, "M"),
                        x = tex_content, fixed = TRUE)
    tex_content <- gsub(pattern = "*SMJ13*",
                        replacement = graph2latex_sm(res13_list$J, "J"),
                        x = tex_content, fixed = TRUE)
    tex_content <- gsub(pattern = "*SMN13*",
                        replacement = graph2latex_sm(res13_list$N, "N"),
                        x = tex_content, fixed = TRUE)
    tex_content <- gsub(pattern = "*SMW13*",
                        replacement = graph2latex_sm(res13_list$W, "W"),
                        x = tex_content, fixed = TRUE)
    tex_content <- gsub(pattern = "*SMZ13*",
                        replacement = graph2latex_sm(res13_list$Z, "Z"),
                        x = tex_content, fixed = TRUE)


    #
    # Parametry etap 2.2
    #
    tex_content <- gsub(pattern = "*QUARTER*", replacement = phases_out$QUARTER, x = tex_content,
                        fixed = TRUE)
    tex_content <- gsub(pattern = "*N*", replacement = phases_out$N, x = tex_content,
                        fixed = TRUE)
    tex_content <- gsub(pattern = "*PROB*", replacement = phases_out$PROB, x = tex_content,
                        fixed = TRUE)
    tex_content <- gsub(pattern = "*ALPHA*", replacement = phases_out$ALPHA, x = tex_content,
                        fixed = TRUE)
    tex_content <- gsub(pattern = "*RES22A*", replacement = phases_out$res22[1], x = tex_content,
                        fixed = TRUE)
    tex_content <- gsub(pattern = "*RES22B*", replacement = phases_out$res22[2], x = tex_content,
                        fixed = TRUE)


    #
    # Parametry etap 3
    #
    tex_content <- gsub(pattern = "*PROBS*",
                        replacement = paste0(phases_out$PROBS, collapse = ", "),
                        x = tex_content,
                        fixed = TRUE)
    tex_content <- gsub(pattern = "*FROMDATE*",
                        replacement = phases_out$FROM_DATE,
                        x = tex_content,
                        fixed = TRUE)
    tex_content <- gsub(pattern = "*PLOTDATES*",
                        replacement = paste0(phases_out$PLOT_DATES, collapse = ", "),
                        x = tex_content,
                        fixed = TRUE)

    #
    # Dodatek (Etap 1.3)
    #
    res13_list <- phases_out$res13_list
    tex_content <- gsub(pattern = "*BIGM13*",
                        replacement = graph2latex_big(res13_list$M, "M"),
                        x = tex_content, fixed = TRUE)
    tex_content <- gsub(pattern = "*BIGJ13*",
                        replacement = graph2latex_big(res13_list$J, "J"),
                        x = tex_content, fixed = TRUE)
    tex_content <- gsub(pattern = "*BIGN13*",
                        replacement = graph2latex_big(res13_list$N, "N"),
                        x = tex_content, fixed = TRUE)
    tex_content <- gsub(pattern = "*BIGW13*",
                        replacement = graph2latex_big(res13_list$W, "W"),
                        x = tex_content, fixed = TRUE)
    tex_content <- gsub(pattern = "*BIGZ13*",
                        replacement = graph2latex_big(res13_list$Z, "Z"),
                        x = tex_content, fixed = TRUE)

    #
    # Log
    #
    tex_content <- sub(pattern = "*LOG*",
                       replacement = log2latex(path = file.path(out_dir, "log.txt")),
                       x = tex_content, fixed = TRUE)

    #
    # Zapis pliku
    #
    tt <- Sys.time()
    dt <- format(tt, "%Y-%m-%d")
    ht <- format(tt, "%H:%M:%S")
    tex_content <- sub(pattern = "*DATE*", replacement = dt, x = tex_content,
                       fixed = TRUE)
    tex_content <- sub(pattern = "*HOUR*", replacement = ht, x = tex_content,
                       fixed = TRUE)
    ftex <- format(tt, "Raport_walidacji_%Y-%m-%d_%H.%M.tex")

    out_fpath <- file.path(out_dir, ftex)
    writeLines(text = tex_content, con = out_fpath, useBytes = TRUE)

    return(out_fpath)
}

generate_pdf <- function(tex_fpath) {
    stopifnot(file.exists(tex_fpath))

    pkg_loginfo(msgs$LATEX_CREATE_PDF)

    main_dir <- getwd()
    setwd(dirname(tex_fpath))
    on.exit({
        setwd(main_dir)
    })

    shell(sprintf("pdflatex \"%s\" > nul", basename(tex_fpath)), wait = TRUE)

    return(gsub("[.]tex$", ".pdf", tex_fpath))
}
