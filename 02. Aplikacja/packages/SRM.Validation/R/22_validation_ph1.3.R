#
# PSE S.A.
# 2018
#
# Plik walidacyjny 1.3 - weryfikacja stabilności wyników estymacji
#

#'
#' Wykonuje fazę 1.3 walidacji
#'
#' @param distr_def definicja rozkładu (lista nazwana).
#' @param out_dir folder do zapisu wygenerowanych wykresów/danych.
#'
#' @return lista nazwana z wartościami res13_list, ...
#'
#' @keywords internal
#' @noRd
#'
validation_phase1_3 <- function(distr_def, out_dir) {
    stopifnot(dir.exists(out_dir))

    #
    # Tworzenie folderu z wynikami
    #
    folder13 <- file.path(out_dir, "etap_1.3")
    dir.create(folder13, recursive = TRUE, showWarnings = FALSE)

    #
    # Weryfikacja stabilności estymacji rozkładów - wykorzystanie obliczen równoległych
    #
    pkg_loginfo(msgs$PHASE_1_3_PLOTS_SAVE)
    cl <- makeCluster(spec = min(c(5, detectCores() - 1)))
    clusterCall(cl, function(x) .libPaths(x), .libPaths())
    clusterEvalQ(cl, {
        loadNamespace("ks")
    })
    clusterExport(cl = cl,
                  varlist = c("kernel_fit_report", "folder13", "distr_def"),
                  envir = environment())

    #
    # Obliczenia
    #
    res13_list <- parLapply(
        cl = cl,
        X = 1:5,
        fun = function(x) {
            switch(as.character(x),
                   "1" = kernel_fit_report(distr_def$epsilon_m$kde_list,
                                           file.path(folder13, "M")),
                   "2" = kernel_fit_report(distr_def$eta_j$kde_list,
                                           file.path(folder13, "J")),
                   "3" = kernel_fit_report(distr_def$eta_n$kde_list,
                                           file.path(folder13, "N")),
                   "4" = kernel_fit_report(distr_def$eta_w$kde_list,
                                           file.path(folder13, "W")),
                   "5" = kernel_fit_report(distr_def$gamma_z$kde_list,
                                           file.path(folder13, "Z")))
        })


    #
    # Zakończenie pracy wątków równoległych
    #
    stopCluster(cl)


    #
    # Rezultat (ścieżki do plików)
    #
    res13_list <- lapply(X = res13_list,
                         FUN = function(x) file.path(".", "etap_1.3", basename(x)))
    names(res13_list) <- c("M", "J", "N", "W", "Z")

    return(list(res13_list = res13_list))
}


#'
#' Symilacja fazy 1.3 przy założeniu że dane już zostały wygenerowane.
#'
#' @param out_dir folder do zapisu wygenerowanych wykresów/danych.
#'
#' @return lista nazwana z wartościami res13_list.
#'
#' @keywords internal
#' @noRd
#'
validation_phase1_3_dummy <- function(out_dir) {
    stopifnot(dir.exists(out_dir))

    pkg_loginfo(msgs$PHASE_1_3_PLOTS_SAVE)

    #
    # Rezultat (ścieżki do plików)
    #
    res13_list <- lapply(X = c("M", "J", "N", "W", "Z"),
                         FUN = function(var) {
                             files <- list.files(file.path(out_dir, "etap_1.3"),
                                                 pattern = sprintf("^%s_.*[.]png", var, "*"),
                                                 full.names = FALSE)
                             idxs <- as.integer(gsub("^[MJNWZ]_([0-9]+)[.]png$", "\\1", files))
                             files <- files[order(idxs)]
                             return(file.path(".", "etap_1.3", files))
                         })
    names(res13_list) <- c("M", "J", "N", "W", "Z")

    return(list(res13_list = res13_list))
}
