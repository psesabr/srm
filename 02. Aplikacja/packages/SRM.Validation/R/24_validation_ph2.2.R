#
# PSE S.A.
# 2018
#
# Plik walidacyjny 2.2 - weryfikacja wielkości próby w metodzie MC
#

#'
#' Wykonuje fazę 2.2 walidacji
#'
#' @param plan zawartośc tabeli plan
#' @param wykon zawartość tabeli wykon
#' @param moc_os zawartość tabeli moc_os
#' @param distr_def definicja rozkładu (lista nazwana).
#' @param config parametry konfiguracyjne (list nazwana)
#' @param seed ziarno generatora liczb losowych.
#' @param out_dir folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return lista nazwana z parametrami QUARTER, N, PROB, ALPHA, res22...
#'
#' @keywords internal
#' @noRd
#'
validation_phase2_2 <- function(plan, wykon, moc_os, distr_def, config, seed,
                                out_dir) {
    stopifnot(dir.exists(out_dir))

    #
    # Parametry obliczeń
    #
    # Wybór kwadransa - max zapotrz. z ostatniego mc
    wykon_1m <- tail(wykon, n = 4 * 24 * 30)
    ti <- which.max(wykon_1m$zapotrz_KSE_wykon)
    QUARTER <- rownames(wykon_1m)[ti]
    N <- 1000 # liczba wykonań
    PROB <- 0.9995 # poziom prawdopodobieństwa zbilansowania systemu
    ALPHA <- 0.05 # poziom istotności dla przedziału ufności szacowanych rezerw

    #
    # Wybór danych dla danego kwadransa
    #
    moc_os_q <- moc_os[QUARTER, ]
    plan_q <- plan[QUARTER, ]

    #
    # Weryfikacja stabilności estymacji rozkładów - wykorzystanie obliczen równoległych
    #
    pkg_loginfo(msgs$PHASE_2_2_PAR_CALC)

    set.seed(seed)
    seedi <- sample(1:10 ^ 8, N)
    cl <- makeCluster(spec = min(c(5, detectCores() - 1)))
    clusterCall(cl, function(x) .libPaths(x), .libPaths())
    clusterEvalQ(cl, {
        loadNamespace("SRM.Utils")
        loadNamespace("SRM.Simulations")
    })
    clusterExport(cl = cl,
                  varlist = c("plan_q", "moc_os_q", "seedi", "config", "distr_def",
                              "PROB", "QUARTER"),
                  envir = environment())

    #
    # Obliczenia
    #
    ret_list <- parLapply(cl = cl, X = 1:N, fun = function(x) {
        set.seed(seedi[x])
        kde_yi <- SRM.Simulations::kde_y(SRM.Utils::q_features(QUARTER),
                                         z_hat = plan_q[, "zapotrz_KSE_plan"],
                                         j_bar = moc_os_q[, "moc_os_JWCDc"],
                                         n_bar = moc_os_q[, "moc_os_nJWCD_bez_wiatr"],
                                         w_bar = moc_os_q[, "moc_os_wiatr"],
                                         distr_def = distr_def,
                                         n = config$MC_SAMPLE)
        ri <- SRM.Simulations::calc_reserve(kde_yi, c(PROB), config$KAPPA_Y)
        return(ri)
    })

    #
    # Zakończenie pracy wątków równoległych
    #
    stopCluster(cl)

    #
    # Mergowanie wyniku
    #
    out_r <- c()
    for (i in seq_along(ret_list)) {
        out_r <- c(out_r, ret_list[[i]])
    }

    #
    # Tworzenie folderu i zapis wyniku
    #
    folder22 <- file.path(out_dir, "etap_2.2")
    dir.create(folder22, recursive = TRUE, showWarnings = FALSE)

    #
    # Przedział ufności
    #
    pkg_loginfo(msgs$PHASE_2_2_RES_SAVE)
    res22 <- normal_fit(out_r = out_r, alpha = ALPHA, filename = file.path(folder22, "hist.png"))

    pkg_logdebug(msgs$PHASE_2_2_DEBUG_INFO, res22[1], res22[2])

    res22mat <- matrix(res22, ncol = 2)
    colnames(res22mat) <- names(res22)
    write.csv2(x = res22mat, file = file.path(folder22, "val.csv"), row.names = FALSE)

    return(list(QUARTER = QUARTER, N = N, PROB = PROB, ALPHA = ALPHA,
                res22 = res22))
}

#'
#' Symilacja fazy 2.2 przy założeniu że dane już zostały wygenerowane.
#'
#' @param wykon zawartość tabeli wykon
#' @param out_dir folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return lista nazwana z parametrami QUARTER, N, PROB, ALPHA, res22...
#'
#' @keywords internal
#' @noRd
#'
validation_phase2_2_dummy <- function(wykon, out_dir) {
    stopifnot(dir.exists(out_dir))

    wykon_1m <- tail(wykon, n = 4 * 24 * 30)
    ti <- which.max(wykon_1m$zapotrz_KSE_wykon)
    QUARTER <- rownames(wykon_1m)[ti]
    N <- 1000 # liczba wykonań
    PROB <- 0.9995 # poziom prawdopodobieństwa zbilansowania systemu
    ALPHA <- 0.05 # poziom istotności dla przedziału ufności szacowanych rezerw

    pkg_loginfo(msgs$PHASE_2_2_PAR_CALC)
    pkg_loginfo(msgs$PHASE_2_2_RES_SAVE)

    res22 <- c(0.0, 0.0)
    names(res22) <- c("Real", "Normal")

    return(list(QUARTER = QUARTER, N = N, PROB = PROB, ALPHA = ALPHA,
                res22 = res22))
}
