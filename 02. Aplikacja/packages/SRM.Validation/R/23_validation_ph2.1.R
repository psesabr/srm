#
# PSE S.A.
# 2018
#
# Plik walidacyjny 2.1 - weryfikacja niezależności rozkładów odchyleń
#

#'
#' Wykonuje fazę 2.1 walidacji
#'
#' @param df_eps jeden z wyników fazy 1.1
#' @param out_dir folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return lista nazwana z parametrami ...
#'
#' @keywords internal
#' @noRd
#'
validation_phase2_1 <- function(df_eps, out_dir) {
    stopifnot(dir.exists(out_dir))

    # Tworzenie folderu z wynikami
    #
    folder21 <- file.path(out_dir, "etap_2.1")
    dir.create(folder21, recursive = TRUE, showWarnings = FALSE)

    #
    # Analiza korelacji miar - generowanie wykresu i współczynników
    #
    pkg_loginfo(msgs$PHASE_2_1_CORR_SAVE)
    png(file.path(folder21, "cor_matrix.png"), width = 2000, height = 2000, pointsize = 30)
    pairs(df_eps, upper.panel = panel_cor, gap = 0,
          cex.axis = 1.5, cex.labels = 2, pch = 16, cex = 1, col = rgb(0, 0, 1, 0.03))
    dev.off()

    return(list())
}

#'
#' Symilacja fazy 2.1 przy założeniu że dane już zostały wygenerowane.
#'
#' @param out_dir folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return lista nazwana z parametrami ...
#'
#' @keywords internal
#' @noRd
#'
validation_phase2_1_dummy <- function(out_dir) {
    stopifnot(dir.exists(out_dir))

    pkg_loginfo(msgs$PHASE_2_1_CORR_SAVE)

    return(list())
}
