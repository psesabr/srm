#
# PSE S.A.
# 2018
#
# Inicjalizacja procesu walidacji
#

#'
#' Handler loggera interpretujący komunikaty logowane podczas walidacji i
#' zapisujący do pliku log.txt do umieszczenia w raporcie.
#'
#' @param msg tekst do zalogowania
#' @param handler środowisko handlera
#' @param ... dodatkowe paramety (np. dry oznacza dodanie handlera do loggera)
#'
#' @return TRUE
#'
#' @keywords internal
#' @noRd
#'
.report_logger <- function(msg, handler, ...) {
    dots <- list(...)
    if (length(dots) && "dry" %in% names(dots)) {
        # dodawanie loggera, inicjalizacja
        fname <- with(handler, file)
        if (exists(fname)) {
            unlink(fname, force = TRUE) # usuń poprzedni log
        }
        assign("start_ts", Sys.time(), envir = handler)
        assign("begin_ts", Sys.time(), envir = handler)
        return(TRUE)
    }

    ts <- Sys.time()
    if (endsWith(msg, "[beg]")) {
        assign("begin_ts", ts, envir = handler)
        lines <- c(
            "================================================================================",
            sprintf("%s | %s", ts, gsub("\\s+\\[beg\\]$", "", msg)),
            "--------------------------------------------------------------------------------")
    } else if (endsWith(msg, "[end]")) {
        ts_diff <- ts - get("begin_ts", envir = handler)
        lines <- c(
            "--------------------------------------------------------------------------------",
            sprintf("%s | %s (%s)", ts, gsub("\\s+\\[end\\]$", "", msg), fmt_timediff(ts_diff)),
            "================================================================================")
    } else if (endsWith(msg, "[fin]")) {
        ts_diff <- ts - get("start_ts", envir = handler)
        lines <- c(
            "================================================================================",
            "--------------------------------------------------------------------------------",
            sprintf("%s | %s (%s)", ts, gsub("\\s+\\[fin\\]$", "", msg), fmt_timediff(ts_diff)),
            "--------------------------------------------------------------------------------",
            "================================================================================")
    } else {
        lines <- c(sprintf("%s | %s", ts, msg))
    }

    con <- file(with(handler, file), open = "at", encoding = "cp1250")
    tryCatch({
        msg <- paste0(paste(lines, collapse = "\n"), "\n")
        msg <- enc2utf8(msg)
        msg <- iconv(msg, from = "UTF-8", to = "cp1250")
        cat(msg, file = con)
    },
    finally = {
        close(con)
    })
}

#'
#' Inicjuje generacje raportu. Tworzy folder roboczy, inicjuje loggera.
#'
#' @param out_dir folder do umieszczenia folderu roboczego
#' @param dummy_folder_name nazwa folderu dummy do użycia (jeśli przekazany)
#'
#' @return ścieżka do folderu roboczego
#'
#' @export
#'
init_report_generation <- function(out_dir, dummy_folder_name = NULL) {
    stopifnot(dir.exists(out_dir))

    if (!file.exists(Sys.which("pdflatex"))) {
        stop(srm_err("VALIDATION_PDFLATEX_NOT_FOUND"))
    }

    if (is.null(dummy_folder_name)) {
        # Tworzenie folderu z wynikami walidacji
        date_ts <- format(x = Sys.time(), "walidacja_%Y-%m-%d_%H.%M")
        folder <- file.path(out_dir, "wyniki", date_ts)
        i <- 2
        while (dir.exists(folder)) {
            folder <- file.path(out_dir, "wyniki", date_ts, "(", i, ")")
            i <- i + 1
        }
        dir.create(folder, recursive = TRUE, showWarnings = FALSE)
    } else {
        folder <- file.path(out_dir, "wyniki", dummy_folder_name)
        stopifnot(dir.exists(folder))
    }

    SRM.Validation_getLogger()$addHandler(.report_logger,
                                          file = file.path(folder, "log.txt"),
                                          formatter = function(record) record$msg)

    pkg_loginfo("%s [beg]", msgs$VALIDATION_INIT_BEGIN)
    pkg_loginfo(msgs$OUTPUT_DIR_CREATION) # po faktycznym utworzeniu by zalogowało się

    return(folder)
}

#'
#' Powinna być wywołana po zakończeniu generowania raportu.
#'
#' Jest to odpowiednik init report generation wywoływany po wygenerowaniu
#'   danych walidacyjnych.
#'
#' @export
#'
stop_report_generation <- function() {
    pkg_loginfo("%s [fin]", msgs$VALIDATION_END)

    SRM.Validation_getLogger()$removeHandler(.report_logger)
}

#'
#' Główna funkcja generująca raport walidacji.
#'
#' @param plan zawartośc tabeli plan
#' @param wykon zawartość tabeli wykon
#' @param moc_os zawartość tabeli moc_os
#' @param distr_def definicja rozkładów do zwalidowania
#' @param folder folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return pełna ścieżka do wygenerowanego raportu
#'
#' @export
#'
generate_validation_report <- function(plan, wykon, moc_os, distr_def, folder) {
    stopifnot(dir.exists(folder))

    pkg_loginfo(msgs$VALIDATION_INIT_READING)
    config <- SRM.Config::get_config_params()$values
    paths <- SRM.Config::get_path()
    seed <- SRM.Config::get_seed()

    pkg_loginfo("%s [end]", msgs$VALIDATION_INIT_END)

    phases_out <- c()
    # Z 20_validation_ph1.1.R
    pkg_loginfo("%s [beg]", msgs$PHASE_1_1_BEGIN)
    phases_out <- c(validation_phase1_1(plan, wykon, moc_os, config, folder),
                    phases_out)
    pkg_loginfo("%s [end]", msgs$PHASE_1_1_END)

    # Z 21_validation_ph1.2.R
    pkg_loginfo("%s [beg]", msgs$PHASE_1_2_BEGIN)
    phases_out <- c(validation_phase1_2(distr_def, folder),
                    phases_out)
    pkg_loginfo("%s [end]", msgs$PHASE_1_2_END)

    # Z 22_validation_ph1.3.R
    pkg_loginfo("%s [beg]", msgs$PHASE_1_3_BEGIN)
    phases_out <- c(validation_phase1_3(distr_def, folder),
                    phases_out)
    pkg_loginfo("%s [end]", msgs$PHASE_1_3_END)


    # Z 23_validation_ph2.1.R
    pkg_loginfo("%s [beg]", msgs$PHASE_2_1_BEGIN)
    phases_out <- c(validation_phase2_1(phases_out$df_eps, folder),
                    phases_out)
    pkg_loginfo("%s [end]", msgs$PHASE_2_1_END)

    # Z 24_validation_ph2.2.R
    pkg_loginfo("%s [beg]", msgs$PHASE_2_2_BEGIN)
    phases_out <- c(validation_phase2_2(plan, wykon, moc_os, distr_def, config, seed, folder),
                    phases_out)
    pkg_loginfo("%s [end]", msgs$PHASE_2_2_END)

    # Z 25_validation_ph3.R
    pkg_loginfo("%s [beg]", msgs$PHASE_3_BEGIN)
    phases_out <- c(validation_phase3(plan, wykon, moc_os, distr_def, config, seed, folder),
                    phases_out)
    pkg_loginfo("%s [end]", msgs$PHASE_3_END)

    # Z 26_validation_rd_db.R
    pkg_loginfo("%s [beg]", msgs$RD_DB_COPY_BEGIN)
    phases_out <- c(validation_rd_db(paths, folder), phases_out)
    pkg_loginfo("%s [end]", msgs$RD_DB_COPY_END)

    stop_report_generation() # przestajemy generować logi

    # Z 27_validation_latex.R
    tex_fpath <- generate_latex(paths, config, phases_out, seed, folder)
    pdf_fpath <- generate_pdf(tex_fpath)
    return(pdf_fpath)
}


#'
#' Funkcja generująca raport walidacji na podstawie już wygenerowanych
#' uprzednio danych do niego.
#'
#' @param plan zawartośc tabeli plan
#' @param wykon zawartość tabeli wykon
#' @param moc_os zawartość tabeli moc_os
#' @param folder folder wynikowy do umieszczenia wyników pośrednich i
#'   wynikowego raportu
#'
#' @return pełna ścieżka do wygenerowanego raportu
#'
#' @export
#'
generate_dummy_report <- function(plan, wykon, moc_os, folder) {
    stopifnot(dir.exists(folder))

    pkg_loginfo(msgs$VALIDATION_INIT_READING)
    config <- SRM.Config::get_config_params()$values
    paths <- SRM.Config::get_path()
    seed <- SRM.Config::get_seed()

    pkg_loginfo("%s [end]", msgs$VALIDATION_INIT_END)

    phases_out <- c()
    # Z 20_validation_ph1.1.R
    pkg_loginfo("%s [beg]", msgs$PHASE_1_1_BEGIN)
    phases_out <- c(validation_phase1_1_dummy(folder), phases_out)
    Sys.sleep(1.0)
    pkg_loginfo("%s [end]", msgs$PHASE_1_1_END)

    # Z 21_validation_ph1.2.R
    pkg_loginfo("%s [beg]", msgs$PHASE_1_2_BEGIN)
    phases_out <- c(validation_phase1_2_dummy(folder), phases_out)
    Sys.sleep(1.0)
    pkg_loginfo("%s [end]", msgs$PHASE_1_2_END)

    # Z 22_validation_ph1.3.R
    pkg_loginfo("%s [beg]", msgs$PHASE_1_3_BEGIN)
    phases_out <- c(validation_phase1_3_dummy(folder), phases_out)
    Sys.sleep(1.0)
    pkg_loginfo("%s [end]", msgs$PHASE_1_3_END)


    # Z 23_validation_ph2.1.R
    pkg_loginfo("%s [beg]", msgs$PHASE_2_1_BEGIN)
    phases_out <- c(validation_phase2_1_dummy(folder), phases_out)
    Sys.sleep(1.0)
    pkg_loginfo("%s [end]", msgs$PHASE_2_1_END)

    # Z 24_validation_ph2.2.R
    pkg_loginfo("%s [beg]", msgs$PHASE_2_2_BEGIN)
    phases_out <- c(validation_phase2_2_dummy(wykon, folder), phases_out)
    Sys.sleep(1.0)
    pkg_loginfo("%s [end]", msgs$PHASE_2_2_END)

    # Z 25_validation_ph3.R
    pkg_loginfo("%s [beg]", msgs$PHASE_3_BEGIN)
    phases_out <- c(validation_phase3_dummy(wykon, folder), phases_out)
    Sys.sleep(1.0)
    pkg_loginfo("%s [end]", msgs$PHASE_3_END)


    # Z 26_validation_rd_db.R
    pkg_loginfo("%s [beg]", msgs$RD_DB_COPY_BEGIN)
    phases_out <- c(validation_rd_db_dummy(folder), phases_out)
    Sys.sleep(1.0)
    pkg_loginfo("%s [end]", msgs$RD_DB_COPY_END)

    stop_report_generation() # przestajemy generować logi

    # Z 27_validation_latex.R
    tex_fpath <- generate_latex(paths, config, phases_out, seed, folder)
    pdf_fpath <- generate_pdf(tex_fpath)
    return(pdf_fpath)
}


#'
#' Otwiera wygenerowany raport za pomocą explorera.
#'
#' @param pdf_fpath ścieżka do raportu
#'
#' @return invisible(NULL)
#'
#' @export
#'
open_pdf_report <- function(pdf_fpath) {
    stopifnot(file.exists(pdf_fpath))

    pdf_fpath <- normalizePath(pdf_fpath)

    pkg_loginfo(msgs$REPORT_OPENNING)
    suppressWarnings({
        shell(sprintf("explorer.exe \"%s\"", pdf_fpath), wait = TRUE)
    })

    invisible(NULL)
}
