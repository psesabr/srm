# Detect proper script_path (you cannot use args yet as they are build with tools in set_env.r)
script_path <- (function() {
    args <- commandArgs(trailingOnly = FALSE)
    script_path <- dirname(sub("--file=", "", args[grep("--file=", args)]))
    if (!length(script_path)) {
        return("R")
    }
    if (grepl("darwin", R.version$os)) {
        base <- gsub("~\\+~", " ", base) # on MacOS ~+~ in path denotes whitespace
    }
    return(normalizePath(script_path))
})()

# Setting .libPaths() to point to libs folder
source(file.path(script_path, "set_env.R"), chdir = TRUE)

config <- load_config()
args <- args_parser()

pkgs <- list.files("packages")

for (pkg in pkgs) {
    pkg_path <- file.path("packages", pkg)
    devtools::document(pkg_path)
    doc_name <- paste0(pkg, ".pdf")
    if (file.exists(doc_name)) file.remove(doc_name)
    system(paste(shQuote(file.path(R.home("bin"), "R")),"CMD", "Rd2pdf", shQuote(pkg_path)))
}

files <- list.files(all.files = TRUE)
temp_dirs <- files[grepl("^\\.Rd2pdf", files)]
unlink(temp_dirs, recursive = TRUE)

counter <- 4 # numer ostatniego dokumentu podstawowego w dokumentacji
for (pkg in pkgs) {
    counter <- counter + 1
    doc_name <- paste0(pkg, ".pdf")
    new_name <- paste0(sprintf("%02d", counter), ". Appendix - pakiet ", doc_name)
    file.rename(from = doc_name, to = file.path("..", "08. Dokumentacja", new_name))
}
