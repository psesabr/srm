# Detect proper script_path (you cannot use args yet as they are build with tools in set_env.r)
script_path <- (function() {
    args <- commandArgs(trailingOnly = FALSE)
    script_path <- dirname(sub("--file=", "", args[grep("--file=", args)]))
    if (!length(script_path)) {
        return("R")
    }
    if (grepl("darwin", R.version$os)) {
        base <- gsub("~\\+~", " ", base) # on MacOS ~+~ in path denotes whitespace
    }
    return(normalizePath(script_path))
})()

# Setting .libPaths() to point to libs folder
source(file.path(script_path, "set_env.R"), chdir = TRUE)

config <- load_config()
args <- args_parser()

#
# ładowanie konfiguracji
#
env_conf <- list()
env_conf$APP_ROOT <- normalizePath(file.path(script_path, ".."), winslash = "/")
env_conf$COMMON_CONFIG_DIR <- file.path(env_conf$APP_ROOT, config$COMMON_CONFIG_DIR)
env_conf$USER_CONFIG_DIR <- file.path(Sys.getenv('HOME'), "SRM", "settings")

library(SRM.Config)

#load_app_config(env_conf)
#set_rd(get_path()$rd)

#SRM.Config:::.check_rd(get_path()$rd, .GlobalEnv)
config_distr <- get_config_params()

library(SRM.DB)
set_conn(get_path()$db)
set_rd(get_path()$rd)

conn = get_conn()

list_tabs(conn)
library(DBI)
plan_df <- dbGetQuery(conn, "select * from plan")

plan_df <- plan_df[plan_df$dzien_ix == 0 & plan_df$punkt_startowy == 0, ]

del_query <- sprintf("DELETE from plan where dzien_ix <> %s OR punkt_startowy <> %s", 0, 0)

rs <- dbSendStatement(conn, del_query)
dbHasCompleted(rs)
dbGetRowsAffected(rs)
dbClearResult(rs)

dzien_ix_vec <- c(0, 1, 2)
start_point_vec <- get_start_points(conn)
mod_cols <- which(!(names(plan_df) %in% c("ts", "punkt_startowy", "dzien_ix")))
total_plan_df <- plan_df

for (dzien_ix in dzien_ixs) {
    for (start_point in start_points) {
        if (!(dzien_ix == 0 & start_point == 0)) {
            print(paste0("dzien ix = ", dzien_ix, ", start_point = ", start_point))
            temp_df <- plan_df
            temp_df[ ,mod_cols] <- temp_df[ ,mod_cols] + apply(temp_df[ ,mod_cols], 2, function(c) {
                halfrange <- 0.01*(dzien_ix + 1)*(1 + 1 - start_point/max(start_points))*sd(c)
                x <- as.integer(round(runif(nrow(plan_df), -halfrange, halfrange), 0))
                print(paste0("sd = ", sd(c), " rozrzut = ", halfrange))
                return(x)
            })
            temp_df$dzien_ix <- dzien_ix
            temp_df$punkt_startowy <- start_point
            total_plan_df <- rbind(total_plan_df, temp_df)
            rm(temp_df)
        }
    }
}
dbWriteTable(conn, name = "plan", value = total_plan_df, overwrite = TRUE, row.names = FALSE)
