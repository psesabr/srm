Narzędzie szacowania wymaganego poziomu rezerwy mocy
Autorzy: Zespół OT Data Modelling EY Polska 2017

0. Projekt jest budowany/rozwijany za pomocą RSuite. 
  Projekt wymaga następujących narzędzi (zakładamy, że są zainstalowany):
  - R w wersji 3.3.x
  - Rtools 

1. Żeby zainstalować RSuite należy
1a. pobrać i zainstalować RSuite CLI (narzędzie od obsługi RSuite w cmd)
  Pakiet instalacyjny (MSI) dostępny na stronie https://rsuite.io/RSuite_Download.php.
1b. następnie należy uruchomić w konsoli (cmd.exe) polecenie
	rsuite install 

2. Do poprawnego zbudowaniu projektu konieczne jest by w ścieżce były dostępne
  narzędzia Rtools. Można sprawdzić ich dostępność w następujący sposób:
	> gcc
	gcc: fatal error: no input files
	compilation terminated.
  Komunikat 'fatal error' pochodzi od gcc, więc gcc jest dostępne.
  Jeśli gcc nie jest dostępne należy ustawić ścieżkę w konsoli w nast. sposób:
	path c:\Rtools\bin;c:\Rtools\mingw_64\bin;%PATH%
  oraz upewnić się, że po tym gcc jest dostępne.

3. Po pobraniu źródeł projektu, w konsoli (cmd.exe) w folderze 02. Aplikacja należy 
  uruchomić polecenie:
	rsuite proj depsinst
  Polecenie zainstaluje wszystkie niezbędne zależności.

4. Następnie należy uruchomić polecenie:
	rsuite proj build
  Polecenie zbuduje i zainstaluje pakiety projektowe narzędzia.

5. Aplikację uruchamiany w trybie wsadowym (osobna instancja R) poprzez plik run.bat znajdujący się w folderze ./02. Aplikacja
6. Aplikację uruchamiany z poziomu RStudio (z załadowanym projektem SRM.Rproj) korzystając ze skryptu ./R/master.R

7. Żeby przygotować paczkę do wdrożenia należy:
7a. zakomitować wszystkie zmiany w projekcie i otagować go (tag powinien składać się z cyfr - jest używany 
  do tagowania wersji pakietów wewnętrznych)
7b. upewnić się, że w folderze settings nie ma plików z ustawieniami dewełopera: settings/{path,config,seed}.yaml
7c. uruchomić polecenie
	rsuite proj zip
  Polecenie przygotuje archiwum zip otagowane i zawierające wszystkie niezbędne do wdrożenia komponenty systemu.
