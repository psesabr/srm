#
# PSE S.A.
# 2018
#
# Testy oszacowania rezerwy
#

context("Testowanie poprawności generacji rezerwy mocy [test_reserve_df]")

library(SRM.Utils)
library(SRM.Simulations)
library(testthat)

source("R/data_load_helpers.R")
source("R/expectations.R")

test_that("Oszacowanie rezerwy mocy daje oczekiwane wartości (parallel)", {
    # Dane wejściowe i ustawienia ...
    lastday <- load_input_lastday_df()
    data_plan <- lastday$data_plan
    data_cap <- lastday$data_cap

    start_point_vec <- sort(unique(data_plan$punkt_startowy))
    day_ix_vec <- sort(unique(data_plan$dzien_ix))

    perc <- 0.95
    mc_sample <- 500000
    kappa <- list(KAPPA_Y = 275,
                  KAPPA_Z = 25,
                  KAPPA_W = 10,
                  KAPPA_M = 10,
                  KAPPA_J = 250,
                  KAPPA_N = -25)
    seed <- 123456
    distr_obj <- load_distr_obj()

    # Funkcja do sprawdzenia poprawności raportowania progresu
    report_sink <- list(task_no = NA, core_no = NA)
    start_f <- function(task_no, core_no) {
        report_sink$task_no <<- task_no
        report_sink$core_no <<- core_no
    }

    for (day_ix in day_ix_vec) {
        for (start_point in start_point_vec) {
            # Generacja prognoz
            plan_df <- data_plan[data_plan$punkt_startowy == start_point & data_plan$dzien_ix == day_ix, ]
            res_df <- reserve_df(perc = perc, plan_df = plan_df, moc_os = data_cap,
                                 mc_sample = mc_sample, kappa = kappa,
                                 distr_obj = distr_obj,
                                 seed = seed,
                                 start_f = start_f)
            # sprawdzenie poprawności raportu
            expect_equal(report_sink$task_no, 24 * 4)
            # sprawdzenie poprawności wyników
            exp_df <- load_df(sprintf("data/reserve_df_exp_%04d %d.csv", start_point, day_ix),
                              row.names = TRUE)
            expect_df_equal(res_df, exp_df)
        }
    }
})


test_that("Oszacowanie rezerwy mocy daje oczekiwane wartości (sequential)", {
    # Dane wejściowe i ustawienia ...
    lastday <- load_input_lastday_df()
    data_plan <- lastday$data_plan
    data_cap <- lastday$data_cap

    start_point_vec <- sort(unique(data_plan$punkt_startowy))
    day_ix_vec <- sort(unique(data_plan$dzien_ix))

    perc <- 0.95
    mc_sample <- 500000
    kappa <- list(KAPPA_Y = 275,
                  KAPPA_Z = 25,
                  KAPPA_W = 10,
                  KAPPA_M = 10,
                  KAPPA_J = 250,
                  KAPPA_N = -25)
    seed <- 123456
    distr_obj <- load_distr_obj()

    for (day_ix in day_ix_vec) {
        for (start_point in start_point_vec) {
            # Generacja prognoz
            plan_df <- data_plan[data_plan$punkt_startowy == start_point & data_plan$dzien_ix == day_ix, ]
            res_df <- reserve_df_sequential(perc = perc, plan_df = plan_df, moc_os = data_cap,
                                 mc_sample = mc_sample, kappa = kappa,
                                 distr_obj = distr_obj,
                                 seed = seed)
            # sprawdzenie poprawności wyników
            exp_df <- load_df(sprintf("data/reserve_df_exp_%04d %d.csv", start_point, day_ix),
                              row.names = TRUE)
            expect_df_equal(res_df, exp_df)
        }
    }
})
