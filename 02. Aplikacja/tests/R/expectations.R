#
# PSE S.A.
# 2018
#
# Funkcje pomocnicze dla testów poprawności budowania rozkładów
#

#'
#' Zamienia vector na napis.
#' @param vec wektor do zamiany na napis
#' @return character
#'
.vec2char <- function(vec) {
    if (length(vec) > 5) {
        return(sprintf("%s ... %s",
                       paste(vec[1:4], collapse = ", "),
                       vec[length(vec)]))
    }
    return(paste(vec, collapse = ", "))
}

#'
#' Expect do sprawdzenia, że zawartość podanej zmiennej w środowiskach jest
#' tożsama.
#'
#' @param res_def wynikowa definicja rozkładów do sprawdzenia
#' @param exp_def oczekiwana deinicja rozkładów
#' @param var_name nazwa zmiennej do sprawdzenia
#'
expect_dist_equal <- function(res_def, exp_def, var_name) {
    res_obj <- res_def[[var_name]]
    exp_obj <- exp_def[[var_name]]

    err <- NULL
    if (!setequal(names(res_obj), names(exp_obj))) {
        err <- sprintf("%s: names c(%s) != c(%s)",
                       var_name,
                       paste(names(exp_obj), collapse = ", "),
                       paste(names(res_obj), collapse = ", "))
    } else if (length(res_obj$map) != length(exp_obj$map)) {
        err <- sprintf("%s: invalid disagregation applied exp. %s elements in map != %s",
                       var_name, length(exp_obj$map), length(res_obj$map))
    } else if (!all(res_obj$map == exp_obj$map)) {
        err <- sprintf("%s: maps do not match at indexes %s",
                       var_name,
                       .vec2char(which(res_obj$map != exp_obj$map)))
    } else if (length(res_obj$kde_list) != length(res_obj$kde_list)) {
        err <- sprintf("%s: kde_list of length %s expected != %s",
                       var_name, length(exp_obj$kde_list), length(res_obj$kde_list))
    } else {
        # sprawdzamy zgodność elementów kde_list
        i <- 1
        while (i < length(res_obj$kde_list)) {
            exp_it <- exp_obj$kde_list[[i]]
            res_it <- res_obj$kde_list[[i]]

            if (!setequal(names(exp_it), names(res_it))) {
                err <- sprintf("%s: [kde it #%s] names c(%s) != c(%s)",
                               var_name, i,
                               paste(names(exp_it), collapse = ", "),
                               paste(names(res_it), collapse = ", "))
                break;
            }

            # zgodność wektorów
            for (nm in c("x", "eval_points", "estimate", "w")) {
                if (length(exp_it[[nm]]) != length(res_it[[nm]])) {
                    err <- sprintf("%s: [kde it #%s] %s vector of length %s expected != %s",
                                   var_name, i, nm,
                                   length(exp_it[[nm]]), length(res_it[[nm]]))
                    break;
                }

                diff_ix <- which(exp_it[[nm]] != res_it[[nm]])
                if (length(diff_ix) > 0) {
                    err <- sprintf("%s: [kde it #%s] %s do not match at indexes %s",
                                   var_name, i, nm, .vec2char(diff_ix))
                    break;
                }
            }
            if (!is.null(err)) {
                break;
            }

            # zgodność wartości skalarnych
            for (nm in c("h", "H", "gridtype", "gridded", "binned")) {
                if (exp_it[[nm]] != res_it[[nm]]) {
                    err <- sprintf("%s: [kde it #%s] %s expected value %s != %s",
                                   var_name, i, nm, exp_it[[nm]], res_it[[nm]])
                    break;
                }
            }
            if (!is.null(err)) {
                break;
            }

            # zgodność wartości cont
            if (!setequal(names(exp_it$cont), names(res_it$cont))) {
                err <- sprintf("%s: [kde it #%s] cont names do not match", var_name, i)
                break;
            }
            if (!all(exp_it$cont == res_it$cont)) {
                err <- sprintf("%s: [kde it #%s] cont do not match at indexes %s",
                               var_name, i, .vec2char(which(exp_it$cont != res_it$cont)))
                break;
            }

            i <- i + 1
        }
    }

    expect(is.null(err), failure_message = err)
}

#'
#' Expect sprawdzający tożsamość ramek danycj.
#'
#' @param obj_df ramka danych do sprawdzenia
#' @param exp_df ramka danych z oczekiwanymi wartościami.
#'
expect_df_equal <- function(obj_df, exp_df) {
    err <- NULL
    if (!setequal(setdiff(colnames(obj_df), "ts"),
                  setdiff(colnames(exp_df), "ts"))) {
        err <- sprintf("Unexpected columns in output %s (expected: %s)",
                       paste(setdiff(colnames(obj_df), "ts"), collapse = ", "),
                       paste(setdiff(colnames(exp_df), "ts"), collapse = ", "))
    } else if (nrow(obj_df) != nrow(exp_df)) {
        err <- sprintf("Unexpected number of records in output %s (expected: %s)",
                       nrow(obj_df), nrow(exp_df))
    } else if (!all(rownames(obj_df) == rownames(exp_df))) {
        err <- sprintf("Rownames in output do not match expected at indexes %s",
                       .vec2char(which(rownames(obj_df) != rownames(exp_df))))
    } else {
        for (nm in setdiff(colnames(exp_df), "ts")) {
            diff_rows <- rownames(obj_df)[which(obj_df[, nm] != exp_df[, nm])]
            if (length(diff_rows) > 0) {
                err <- sprintf("Output variable '%s' has unexpected value(s) for %s",
                               nm, .vec2char(diff_rows))
                break;
            }
        }
    }

    expect(is.null(err), failure_message = err)
}
